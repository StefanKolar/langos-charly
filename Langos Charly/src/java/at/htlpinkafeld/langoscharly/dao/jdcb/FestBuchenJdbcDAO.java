package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.interfaces.FestBuchenDAO;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 *
 * @author kevinunger
 */
public class FestBuchenJdbcDAO extends BaseJdbcDAO<Fest> implements FestBuchenDAO{
    
    
    public FestBuchenJdbcDAO() 
    {
        super("Feste", "FestID");
    }

    @Override
    protected Fest getPojoFromResultSet(ResultSet result) throws SQLException {
        int festBuchenID = result.getInt("FestID");
        int userID = result.getInt("UserID");
        String festName = result.getString("Name");
        String vorname = result.getString("Vorname");
        String nachname = result.getString("Nachname");
        String email = result.getString("Email");
        String telefonnummer = result.getString("Telefonnummer");
        String ort = result.getString("Ort");
        int plz = result.getInt("PLZ");
        String adresse = result.getString("Adresse");
        LocalDateTime aufstellDatum = result.getObject("Aufstelldatum", LocalDateTime.class);
        LocalDateTime veranstaltungsBeginn = result.getObject("Veranstalltungbeginn", LocalDateTime.class);
        LocalDateTime veranstaltungsEnde = result.getObject("Veranstalltungsende", LocalDateTime.class);
        String stromanschluss = result.getString("Stromanschluss");
        int besucherAnzahl = result.getInt("Besucheranzahl");
        int speisekarte = result.getInt("Speisekarten");
        int kassa = result.getInt("Kassa");
        String anmerkungen = result.getString("Anmerkungen");
       
        boolean besteatigt = result.getBoolean("Bestätigt");
        boolean abgeschlossen = result.getBoolean("Abgeschlossen");
        
        return new Fest(festBuchenID, userID, festName, vorname, nachname, email, telefonnummer, ort, plz, adresse, aufstellDatum, veranstaltungsBeginn, veranstaltungsEnde, stromanschluss, besucherAnzahl, speisekarte, kassa, anmerkungen, besteatigt, abgeschlossen);
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Fest t) throws SQLException 
    {
        String sql = "UPDATE " + super.getTablename() + " set UserID = ?, Name = ?, Vorname = ?, Nachname = ?, Email = ?, Telefonnummer = ?, Ort = ?, PLZ = ?, Adresse = ?, Aufstelldatum = ?, Veranstalltungbeginn = ?, Veranstalltungsende = ?, Stromanschluss = ?, Besucheranzahl = ?, Speisekarten = ?, Kassa = ?, Anmerkungen = ?, Bestätigt = ?, Abgeschlossen = ?  where " + getPkName() + " = ?";
        
        PreparedStatement pStatement = c.prepareStatement(sql);
        
        pStatement.setInt(1, t.getUserID());
        pStatement.setString(2, t.getFestName());
        pStatement.setString(3, t.getVorname());
        pStatement.setString(4, t.getNachname());
        pStatement.setString(5, t.getEmail());
        pStatement.setString(6, t.getTelefonnummer());
        pStatement.setString(7, t.getOrt());
        pStatement.setInt(8, t.getPlz());
        pStatement.setString(9, t.getAdresse());
        pStatement.setDate(10, new java.sql.Date(t.getAufstellDatum().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
        pStatement.setDate(11, new java.sql.Date(t.getVeranstaltungsBeginn().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
        pStatement.setDate(12, new java.sql.Date(t.getVeranstaltungsEnde().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
        pStatement.setString(13, t.getStromanschluss());
        pStatement.setInt(14, t.getBesucherAnzahl());
        pStatement.setInt(15, t.getSpeisekarte());
        pStatement.setInt(16, t.getKassa());
        pStatement.setString(17, t.getAnmerkungen());
        pStatement.setBoolean(18, t.isBestaetigt());
        pStatement.setBoolean(19, t.isAbgeschlossen());
        pStatement.setInt(20, t.getFestBuchenID());
        
        return pStatement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Fest t) throws SQLException 
    {
        String sql = "INSERT INTO " + super.getTablename() + " (UserID, Name, Vorname, Nachname, Email, Telefonnummer, Ort, PLZ, Adresse, Aufstelldatum, Veranstalltungbeginn, Veranstalltungsende, Stromanschluss, Besucheranzahl, Speisekarten, Kassa, Anmerkungen, Bestätigt, Abgeschlossen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        pStatement.setInt(1, t.getUserID());
        pStatement.setString(2, t.getFestName());
        pStatement.setString(3, t.getVorname());
        pStatement.setString(4, t.getNachname());
        pStatement.setString(5, t.getEmail());
        pStatement.setString(6, t.getTelefonnummer());
        pStatement.setString(7, t.getOrt());
        pStatement.setInt(8, t.getPlz());
        pStatement.setString(9, t.getAdresse());
        pStatement.setObject(10, t.getAufstellDatum());
        pStatement.setObject(11, t.getVeranstaltungsBeginn());
        pStatement.setObject(12, t.getVeranstaltungsEnde());
        pStatement.setString(13, t.getStromanschluss());
        pStatement.setInt(14, t.getBesucherAnzahl());
        pStatement.setInt(15, t.getSpeisekarte());
        pStatement.setInt(16, t.getKassa());
        pStatement.setString(17, t.getAnmerkungen());
        pStatement.setBoolean(18, t.isBestaetigt());
        pStatement.setBoolean(19, t.isAbgeschlossen());
        
        return pStatement;
    }
 
}
