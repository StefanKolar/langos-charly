package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.pojo.Identifiable;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class BaseJdbcDAO<T extends Identifiable> {

    private final String TABLENAME;
    private final String PKNAME;

    private final Map< Integer, T> map = new HashMap<>();

    public BaseJdbcDAO(String tablename, String pkName) {
        this.TABLENAME = tablename;
        this.PKNAME = pkName;
    }

    public boolean contains(int id) {
        return map.containsKey(id);
    }

    public void put(T object) {
        map.put(object.getID(), object);
    }

    public T get(int id) {
        return map.get(id);
    }

    public void remove(int id) {
        map.remove(id);
    }

    protected abstract T getPojoFromResultSet(ResultSet result) throws SQLException;

    protected abstract PreparedStatement getUpdateStatement(Connection c, T t) throws SQLException;

    protected abstract PreparedStatement getInsertStatement(Connection c, T t) throws SQLException;
    
    /**
     * Override to define what Columns you want to get out of a resultset 
     * @return String with the defined Columns
     */
    protected String readSQLStatment(){
        return "SELECT * FROM " + TABLENAME;
    }

    public String getTablename() {
        return TABLENAME;
    }

    public String getPkName() {
        return PKNAME;
    }

    private PreparedStatement getPreparedStatement(Connection c, String sql, int id) throws SQLException {
        PreparedStatement stmt = c.prepareStatement(sql);
        stmt.setInt(1, id);
        return stmt;
    }

    public boolean delete(T t) {

        if (PKNAME != null) {

            if (t.getID() < 0) {
                return false;
            }

            String sql = "DELETE FROM " + TABLENAME + " WHERE " + PKNAME + " = ?";
            try (
                    Connection connection = ConnectionManager.getInstance().getConnection();
                    PreparedStatement stmt = getPreparedStatement(connection, sql, t.getID())) {

                if (stmt.executeUpdate() > 0) {
                    remove(t.getID());
                    return true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(BaseJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
                throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
            }

        }

        return false;
    }

    public T read(int id) {

        if (PKNAME != null) {

            T t = null;
            String sql = readSQLStatment() + " WHERE " + PKNAME + " = ? LIMIT 1";
            try (Connection connection = ConnectionManager.getInstance().getConnection();
                    PreparedStatement stmt = getPreparedStatement(connection, sql, id)) {
                ResultSet result = stmt.executeQuery();

                if (result.next()) {
                    if (contains(id)) {
                        return get(id);
                    }
                    t = getPojoFromResultSet(result);
                    put(t);
                }
            } catch (SQLException ex) {
                Logger.getLogger(BaseJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
                throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
            }
            if (t == null) {
                return null;
            }

            return t;

        }
        return null;
    }

    public List<T> readAll() {
        List<T> results = new LinkedList<>();
        String sql = readSQLStatment();
        try (Connection connection = ConnectionManager.getInstance().getConnection();
                Statement stmt = connection.createStatement();
                ResultSet result = stmt.executeQuery(sql)) {

            while (result.next()) {
                if (PKNAME != null) {
                    int TID = result.getInt(PKNAME);
                    if (contains(TID)) {
                        results.add(get(TID));
                    } else {
                        T t = getPojoFromResultSet(result);
                        results.add(t);
                        put(t);
                    }
                } else {
                    results.add(getPojoFromResultSet(result));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }

    public boolean update(T t) {
        if (t.getID() < 0) {
            return false;
        }

        try (Connection connection = ConnectionManager.getInstance().getConnection();
                PreparedStatement stmt = getUpdateStatement(connection, t)) {
            if (stmt.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

    public boolean create(T t) {
        try (Connection connection = ConnectionManager.getInstance().getConnection();
                PreparedStatement stmt = getInsertStatement(connection, t);
                ResultSet genKeys = (stmt.executeUpdate() == 1) ? stmt.getGeneratedKeys() : null) {

            if (genKeys != null && genKeys.next()) {
                t.setID(genKeys.getInt(1));
                if (PKNAME != null) {
                    put(t);
                }
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseJdbcDAO.class).log(Level.FATAL, ex.getMessage());
            throw new UserException(FacesMessage.SEVERITY_ERROR, String.valueOf(ex.getErrorCode()), ex);
        }
        return false;
    }
}
