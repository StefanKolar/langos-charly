package at.htlpinkafeld.langoscharly.pojo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author DominikT
 */
public class Bestellung implements Identifiable {
    private int bestellID;
    private int userID;
    private LocalDate datum;
    private String ort;
    private int plz;
    private String adresse;
    private boolean bestaetigt;

    public Bestellung(int bestellID, int userID, LocalDate datum, String ort, int plz, String adresse, boolean bestaetigt) {
        this(bestellID, userID, datum, ort, plz, adresse);
        this.bestaetigt = bestaetigt;
    }
    
    public Bestellung(int bestellID, int userID, LocalDate datum, String ort, int plz, String adresse) {
        this(userID, datum, ort, plz, adresse);
        this.bestellID = bestellID;
    }

    public Bestellung(int userID, LocalDate datum, String ort, int plz, String adresse) {
        this.userID = userID;
        this.datum = datum;
        this.ort = ort;
        this.plz = plz;
        this.adresse = adresse;
    }
    
    public Bestellung() {
    }
    
    @Override
    public void setID(int id) {
        this.bestellID = id;
    }

    @Override
    public int getID() {
        return this.bestellID;
    }

    public int getBestellID() {
        return bestellID;
    }

    public void setBestellID(int bestellID) {
        this.bestellID = bestellID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public boolean isBestaetigt() {
        return bestaetigt;
    }

    public void setBestaetigt(boolean bestaetigt) {
        this.bestaetigt = bestaetigt;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.bestellID;
        hash = 71 * hash + this.userID;
        hash = 71 * hash + Objects.hashCode(this.datum);
        hash = 71 * hash + Objects.hashCode(this.ort);
        hash = 71 * hash + this.plz;
        hash = 71 * hash + Objects.hashCode(this.adresse);
        hash = 71 * hash + (this.bestaetigt ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bestellung other = (Bestellung) obj;
        if (this.bestellID != other.bestellID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.plz != other.plz) {
            return false;
        }
        if (this.bestaetigt != other.bestaetigt) {
            return false;
        }
        if (!Objects.equals(this.ort, other.ort)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.datum, other.datum)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bestellung{" + "bestellID=" + bestellID + ", userID=" + userID + ", datum=" + datum + ", ort=" + ort + ", plz=" + plz + ", adresse=" + adresse + ", bestaetigt=" + bestaetigt + '}';
    }

}
