package at.htlpinkafeld.langoscharly.gui.filter;

import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class AdminFilter implements Filter {

    private static final Logger LOGGER = Logger.getLogger(AdminFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        LOGGER.debug("Admin Filter:");

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);

        if (session != null && loggedIn) {
            User user = (User) session.getAttribute("user");

            if (user.getRolle().isAdmin()) {
                chain.doFilter(req, res);
                LOGGER.debug("Admin Filter leitet weiter");
            } else {
                LOGGER.debug("Admin Filter verhindert weiterleitung");
                String url = LangosCharlyService.BASE_URL;
                response.sendRedirect(response.encodeRedirectURL(url));
            }

        } else {
            LOGGER.debug("Session oder User ist nicht gesetzt");
            String url = LangosCharlyService.BASE_URL;
            response.sendRedirect(response.encodeRedirectURL(url));
        }
    }

    @Override
    public void destroy() {
    }

}
