package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import java.util.List;

/**
 *
 * @author DominikT
 */
public interface BestellpositionDAO extends BaseDAO<Bestellposition> {
    public List<Bestellposition> readBestellpositionenByBestellID(int id);
}