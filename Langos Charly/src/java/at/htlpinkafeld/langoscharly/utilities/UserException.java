package at.htlpinkafeld.langoscharly.utilities;

import javax.faces.application.FacesMessage.Severity;

/**
 *
 * @author Kolar
 */
public class UserException extends RuntimeException {

    private final Severity severity;

    public UserException(Severity severity, String message, Throwable cause) {
        super(message, cause);
        this.severity = severity;
    }

    public UserException(Severity severity, Throwable cause) {
        super(cause);
        this.severity = severity;
    }

    public Severity getSeverity() {
        return severity;
    }

    @Override
    public String toString() {
        return "UserException{" + getMessage() + "   " + getCause().getMessage() + '}';
    }
    

}
