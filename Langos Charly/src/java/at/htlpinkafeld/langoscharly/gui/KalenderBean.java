package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Kolar
 */
public class KalenderBean implements BaseBean {

    private ScheduleModel kalender;
    private LangosCharlyService service;

    public KalenderBean() {
    }

    @PostConstruct
    public void init() {
        kalender = getEvents();
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public ScheduleModel getKalender() {
        return kalender;
    }

    public void setKalender(ScheduleModel kalender) {
        this.kalender = kalender;
    }
    
    public int getCurrentYear(){
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    private DefaultScheduleModel getEvents() {
        DefaultScheduleModel events = new DefaultScheduleModel();

        try {
            if (service.getBestellungen() != null) {
                for (Bestellung bestellung : service.getBestellungen()) {
                    DefaultScheduleEvent event = new DefaultScheduleEvent("Lieferung: " + bestellung.getOrt(), Date.valueOf(bestellung.getDatum()), Date.valueOf(bestellung.getDatum()), bestellung);
                    event.setDescription("Bestellung");
                    event.setAllDay(true);
                    if (bestellung.isBestaetigt()) {
                        event.setStyleClass("besteatigt");
                    } else {
                        event.setStyleClass("unbesteatigt");
                    }
                    events.addEvent(event);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (service.getFeste() != null) {
                for (Fest fest : service.getFeste()) {
                    DefaultScheduleEvent event = new DefaultScheduleEvent("Fest: " + fest.getFestName(), new java.sql.Date(fest.getVeranstaltungsBeginn().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()), new java.sql.Date(fest.getVeranstaltungsEnde().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()), fest);
                    event.setDescription("Fest");
                    event.setAllDay(true);
                    if (fest.isBestaetigt()) {
                        event.setStyleClass("besteatigt");
                    } else {
                        event.setStyleClass("unbesteatigt");
                    }
                    events.addEvent(event);
                }
            }
        } catch (Exception e) {
        }

        return events;
    }

    public void onEventSelect(SelectEvent e) throws MessagingException, IOException {
        ScheduleEvent event = (ScheduleEvent) e.getObject();

        if (event.getData() instanceof Bestellung) {

            Bestellung bestellung = (Bestellung) event.getData();

            ValueExpression vex = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createValueExpression(FacesContext.getCurrentInstance().getELContext(), "#{bestellungBean}", BestellungBean.class);

            BestellungBean bean = (BestellungBean) vex.getValue(FacesContext.getCurrentInstance().getELContext());
            bean.setBestellungForDetails(bestellung);

            FacesContext.getCurrentInstance().getExternalContext().redirect( ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).encodeRedirectURL("/admin/bestellungDetails.xhtml?faces-redirect=true"));
        }

        if (event.getData() instanceof Fest) {

            Fest fest = (Fest) event.getData();

            ValueExpression vex = FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createValueExpression(FacesContext.getCurrentInstance().getELContext(), "#{festBuchenBean}", FestBuchenBean.class);

            FestBuchenBean bean = (FestBuchenBean) vex.getValue(FacesContext.getCurrentInstance().getELContext());
            bean.setBuchung(fest);

            FacesContext.getCurrentInstance().getExternalContext().redirect( ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).encodeRedirectURL("/admin/festDetails.xhtml?faces-redirect=true"));
        }

    }

    public void onEventMove(ScheduleEntryMoveEvent e) {
        if (e.getScheduleEvent().getData() instanceof Bestellung) {

            Bestellung bestellung = (Bestellung) e.getScheduleEvent().getData();

            if (!bestellung.isBestaetigt()) {

                LocalDate date = bestellung.getDatum();

                date = date.plusDays(e.getDayDelta());

                bestellung.setDatum(date);

                service.updateBestellung(bestellung);
            }

        }

    }

}
