package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.interfaces.LangosDAO;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class LangosJdbcDAO extends BaseJdbcDAO<Langos> implements LangosDAO {

    public LangosJdbcDAO() {
        super("Langos", "LangosID");
    }

    @Override
    protected String readSQLStatment() {
        return "SELECT " + getPkName() + ", Name, Beschreibung, Allergene, Mindestbestand, Verfügbar, Bemerkung FROM " + super.getTablename();
    }
    
    @Override
    protected Langos getPojoFromResultSet(ResultSet result) throws SQLException {

        int langosID = result.getInt(getPkName());
        String name = result.getString("Name");
        String beschreibung = result.getString("Beschreibung");
        String allergene = result.getString("Allergene");
        int mindestbestand = result.getInt("Mindestbestand");
        boolean verfuegbar = result.getBoolean("Verfügbar");
        String bemerkung = result.getString("Bemerkung");

        return new Langos(langosID, name, beschreibung, allergene, mindestbestand, verfuegbar, bemerkung);

    }

    @Override
    public InputStream readLangosImageByID(int id) {

        String sql = "SELECT Bild FROM " + super.getTablename() + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        return result.getBinaryStream("Bild");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return null;
    }

    @Override
    public boolean updateImage(int id, InputStream bild) {

        String sql = "UPDATE " + super.getTablename() + " set Bild = ?" + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setBinaryStream(1, bild);
                statement.setInt(2, id);
                if (statement.executeUpdate() > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Langos l) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Name = ?, Beschreibung = ?, Allergene = ?, Mindestbestand = ?, Verfügbar = ?, Bemerkung = ? where " + getPkName() + " = ?";

        PreparedStatement statement = c.prepareStatement(sql);
        statement.setString(1, l.getName());
        statement.setString(2, l.getBeschreibung());
        statement.setString(3, l.getAllergene());
        statement.setInt(4, l.getMindestbestand());
        statement.setBoolean(5, l.isVerfuegbar());
        statement.setString(6, l.getBemerkung());
        statement.setInt(7, l.getID());

        return statement;

    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Langos l) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Name, Beschreibung, Allergene, Mindestbestand, Menge, Verfügbar, Bemerkung) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, l.getName());
        statement.setString(2, l.getBeschreibung());
        statement.setString(3, l.getAllergene());
        statement.setInt(4, l.getMindestbestand());
        statement.setInt(5, l.getMenge());
        statement.setBoolean(6, l.isVerfuegbar());
        statement.setString(7, l.getBemerkung());

        return statement;
    }

    @Override
    public int readMengeByID(int id) {
        String sql = "SELECT Menge FROM " + super.getTablename() + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        return result.getInt("Menge");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return 0;
    }

    @Override
    public boolean updateMenge(int id, int menge) {
        String sql = "UPDATE " + super.getTablename() + " set Menge = ?" + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, menge);
                statement.setInt(2, id);
                if (statement.executeUpdate() > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

}
