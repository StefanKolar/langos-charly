package at.htlpinkafeld.langoscharly.pojo;

import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class Information implements Identifiable {

    private int informationID;
    private String beschreibung;
    private String text;

    public Information(int informationID, String beschreibung, String text) {
        this(beschreibung, text);
        this.informationID = informationID;

    }

    @Override
    public void setID(int id) {
        this.informationID = id;
    }

    @Override
    public int getID() {
        return this.informationID;
    }

    public Information(String beschreibung, String text) {
        this.beschreibung = beschreibung;
        this.text = text;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.informationID;
        hash = 89 * hash + Objects.hashCode(this.beschreibung);
        hash = 89 * hash + Objects.hashCode(this.text);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Information other = (Information) obj;
        if (this.informationID != other.informationID) {
            return false;
        }
        if (!Objects.equals(this.beschreibung, other.beschreibung)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Information{" + "informationID=" + informationID + ", beschreibung=" + beschreibung + ", text=" + text + '}';
    }
}
