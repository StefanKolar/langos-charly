package at.htlpinkafeld.langoscharly.gui.filter;

import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.IOException;
import org.apache.log4j.Logger;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author Kolar
 */
public class StatistikFilter implements Filter {

    private static final Logger logger = Logger.getLogger(StatistikFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);

        if (session != null && loggedIn) {
            User user = (User) session.getAttribute("user");

            if (user.getRolle().isStatisik()) {
                chain.doFilter(req, res);
                logger.debug("Statistik Filter leitet weiter");
            } else {
                logger.debug("Statistik Filter verhindert weiterleitung");
                String url = LangosCharlyService.BASE_URL;
                response.sendRedirect(response.encodeRedirectURL(url));
            }

        }
    }

    @Override
    public void destroy() {
    }

}
