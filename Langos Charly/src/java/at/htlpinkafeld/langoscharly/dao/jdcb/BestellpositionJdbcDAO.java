package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.interfaces.BestellpositionDAO;
import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author DominikT
 */
public class BestellpositionJdbcDAO extends BaseJdbcDAO<Bestellposition> implements BestellpositionDAO {

    public BestellpositionJdbcDAO() {
        super("Bestellpositionen", "BestellpositionID");
    }

    @Override
    protected Bestellposition getPojoFromResultSet(ResultSet result) throws SQLException {

        int bestellpositionID = result.getInt(super.getPkName());
        int bestellID = result.getInt("BestellID");
        int langosID = result.getInt("LangosID");
        int menge = result.getInt("Menge");

        return new Bestellposition(bestellpositionID, bestellID, langosID, menge);
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Bestellposition b) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set BestellID = ?, LangosID = ?, Menge = ? where " + getPkName() + " = ?";

        PreparedStatement pStatement = c.prepareStatement(sql);

        pStatement.setInt(1, b.getBestellID());
        pStatement.setInt(2, b.getLangosID());
        pStatement.setInt(3, b.getMenge());

        return pStatement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Bestellposition b) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (BestellID, LangosID, menge) VALUES (?,?,?)";
        PreparedStatement pStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        pStatement.setInt(1, b.getBestellID());
        pStatement.setInt(2, b.getLangosID());
        pStatement.setInt(3, b.getMenge());

        return pStatement;
    }

    @Override
    public List<Bestellposition> readBestellpositionenByBestellID(int id) {
        List<Bestellposition> bestellpositionList = new LinkedList<>();

        String sql = "SELECT * from " + super.getTablename() + " where BestellID = ?";

        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        if (contains(result.getInt(getPkName()))) {
                            bestellpositionList.add(get(result.getInt(getPkName())));
                        } else {
                            bestellpositionList.add(getPojoFromResultSet(result));
                        }
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return bestellpositionList;
    }

}
