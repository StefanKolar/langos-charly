package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author DominikT
 */
public class LagerbestandBean implements BaseBean {

    private LangosCharlyService service;
    private List<Langos> langosList = new LinkedList<>();
    private int index;
    private int bestand;
    private boolean unterschritten;
    private int langosID;
    private int menge;
    public LagerbestandBean() {
    }

    @PostConstruct
    public void init() {
        this.langosList = this.service.getLangos();
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return this.service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getBestand() {
        return bestand;
    }

    public void setBestand(int bestand) {
        this.bestand = bestand;
    }

    public List<Langos> getLangosList() {
        return langosList;
    }

    public void setLangosList(List<Langos> langosList) {
        this.langosList = langosList;
    }

    public int getLangosID() {
        return langosID;
    }

    public void setLangosID(int langosID) {
        this.langosID = langosID;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }
    
    public void increaseBestand(int id) {
        getLangosByID(id).setMenge(getLangosByID(id).getMenge() + this.bestand);
    }

    public void decreaseBestand(int id) {
        getLangosByID(id).setMenge(getLangosByID(id).getMenge() - this.bestand);
        this.unterschritten = isUnterschritten();

        if (this.unterschritten) {
            try {
                sendMail(getLangosByID(id));
            } catch(Exception e) {
                
            }  
        }
    }
    

//    public void decreaseBestand2() {
//        for (Langos l : langosList) {
//            for (Entry<Integer, Integer> e : mengenMap.entrySet()) {
//                if (l.getID() == e.getKey()) {
//                    getLangosByID(l.getID()).setMenge(getLangosByID(l.getID()).getMenge() - e.getValue());
//                    
//                    this.unterschritten = isUnterschritten();
//
//                    if (this.unterschritten) {
//                        sendMail(getLangosByID(l.getID()));
//                    }
//                }
//            }
//        }
//    }

    public Langos getLangosByID(int id) {
        for (Langos l : langosList) {
            if (l.getID() == id) {
                return l;
            }
        }
        return null;
    }

    public boolean isUnterschritten() {
        for (Langos l : langosList) {
            if (l.getMenge() < l.getMindestbestand()) {
                return true;
            }
        }
        return false;
    }

    public void sendMail(Langos l) {
        StringBuilder sb = new StringBuilder();

        String text = service.getInformation(9).getText();
        if (isUnterschritten()) {
            sb.append(l.getName()).append("     ").append(l.getMindestbestand()).append("     ").append(l.getMenge());
        }
        text = text.replace("DETAILS", sb.toString());

        MailClient.sendMail("Lagerbestandwarnung", text);
    }

}
