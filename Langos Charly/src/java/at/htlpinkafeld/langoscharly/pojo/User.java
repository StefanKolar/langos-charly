package at.htlpinkafeld.langoscharly.pojo;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import java.io.InputStream;
import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class User implements Identifiable {

    private int userID;
    private InputStream bild;
    private String firmenname;
    private Typ typ;
    private String vorname, nachname;
    private Rolle rolle;
    private String email;
    private String passwort;
    private String telefonnummer;
    private String adresse;
    private int plz;
    private String ort;

    public User(int userID, String firmenname, Typ typ, String vorname, String nachname, Rolle rolle, String email, String passwort, String telefonnummer, String adresse, int plz, String ort) {
        this(firmenname, typ, vorname, nachname, rolle, email, passwort, telefonnummer, adresse, plz, ort);
        this.userID = userID;
    }

    public User(String firmenname, Typ typ, String vorname, String nachname, Rolle rolle, String email, String passwort, String telefonnummer, String adresse, int plz, String ort) {
        this(firmenname, typ, vorname, nachname, email, passwort, telefonnummer, adresse, plz, ort);
        this.rolle = rolle;

    }

    public User(InputStream bild, String firmenname, Typ typ, String vorname, String nachname, String email, String passwort, String telefonnummer, String adresse, int plz, String ort) {
        this(firmenname, typ, vorname, nachname, email, passwort, telefonnummer, adresse, plz, ort);
        this.bild = bild;
    }
    
    public User(String firmenname, Typ typ, String vorname, String nachname, String email, String passwort, String telefonnummer, String adresse, int plz, String ort) {
        this.firmenname = firmenname;
        this.typ = typ;
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.passwort = passwort;
        this.telefonnummer = telefonnummer;
        this.adresse = adresse;
        this.plz = plz;
        this.ort = ort;
    }
    

    public User() {
    }

    @Override
    public int getID() {
        return userID;
    }

    @Override
    public void setID(int userID) {
        this.userID = userID;
    }

    public InputStream getBild() {
        return DAOFactory.getInstance().readUserImageByID(userID);
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    public String getFirmenname() {
        return firmenname;
    }

    public void setFirmenname(String Firmenname) {
        this.firmenname = Firmenname;
    }

    public Typ getTyp() {
        return typ;
    }

    public void setTyp(Typ typ) {
        this.typ = typ;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public void setRolle(Rolle rolle) {
        this.rolle = rolle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.userID;
        hash = 53 * hash + Objects.hashCode(this.bild);
        hash = 53 * hash + Objects.hashCode(this.firmenname);
        hash = 53 * hash + Objects.hashCode(this.typ);
        hash = 53 * hash + Objects.hashCode(this.vorname);
        hash = 53 * hash + Objects.hashCode(this.nachname);
        hash = 53 * hash + Objects.hashCode(this.rolle);
        hash = 53 * hash + Objects.hashCode(this.email);
        hash = 53 * hash + Objects.hashCode(this.passwort);
        hash = 53 * hash + Objects.hashCode(this.telefonnummer);
        hash = 53 * hash + Objects.hashCode(this.adresse);
        hash = 53 * hash + this.plz;
        hash = 53 * hash + Objects.hashCode(this.ort);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userID != other.userID) {
            return false;
        }
        if (this.plz != other.plz) {
            return false;
        }
        if (!Objects.equals(this.firmenname, other.firmenname)) {
            return false;
        }
        if (!Objects.equals(this.vorname, other.vorname)) {
            return false;
        }
        if (!Objects.equals(this.nachname, other.nachname)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.passwort, other.passwort)) {
            return false;
        }
        if (!Objects.equals(this.telefonnummer, other.telefonnummer)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.ort, other.ort)) {
            return false;
        }
        if (!Objects.equals(this.bild, other.bild)) {
            return false;
        }
        if (this.typ != other.typ) {
            return false;
        }
        if (!Objects.equals(this.rolle, other.rolle)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "userID=" + userID + ", bild=" + bild + ", firmenname=" + firmenname + ", typ=" + typ + ", vorname=" + vorname + ", nachname=" + nachname + ", rolle=" + rolle + ", email=" + email + ", passwort=" + passwort + ", telefonnummer=" + telefonnummer + ", adresse=" + adresse + ", plz=" + plz + ", ort=" + ort + '}';
    }

   
}
