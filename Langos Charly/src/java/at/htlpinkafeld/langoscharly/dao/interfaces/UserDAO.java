package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.User;
import java.io.InputStream;

/**
 *
 * @author Kolar
 */
public interface UserDAO extends BaseDAO<User>{
    
    public User readUserByEmail(String email);
    public InputStream readUserImageByID(int id);
    public boolean updateImage(int id, InputStream bild);
    
}
