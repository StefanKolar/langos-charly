package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Identifiable;
import java.util.List;

/**
 *
 * @author Kolar
 */

public interface BaseDAO<T extends Identifiable> {

    public boolean create(T object);

    public T read(int id);

    public List<T> readAll();

    public boolean update(T object);

    public boolean delete(T object);

}
