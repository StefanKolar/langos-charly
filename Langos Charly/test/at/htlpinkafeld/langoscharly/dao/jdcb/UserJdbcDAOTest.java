package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Kolar
 */
public class UserJdbcDAOTest implements TestDAOClass {

    private List<User> users = new LinkedList<>();
    private Rolle admin, gast;

    @Before
    public void init() {
        admin = DAOFactory.getInstance().readRolle(1);
        gast = DAOFactory.getInstance().readRolle(6);
        users.add(new User(11, "Test1", Typ.FIRMA, "", "", admin, "test1@lustig.at", "12345678", "0664", "", 7423, ""));
        users.add(new User(12, "Test2", Typ.PRIVAT, "", "", gast, "test2@lustig.at", "12345678", "0664", "", 7423, ""));
        users.add(new User(13, "Test3", Typ.VEREIN, "", "", admin, "test3@lustig.at", "12345678", "0664", "", 7423, ""));
        users.add(new User(14, "Test4", Typ.FIRMA, "", "", admin, "test4@lustig.at", "12345678", "0664", "", 7423, ""));

        for (User user : users) {
            DAOFactory.getInstance().createUser(user);
        }
    }

    @After
    public void delete() {
        for (User user : users) {
            DAOFactory.getInstance().deleteUser(user);
        }
    }

    @Test
    public void testReadUserByEmail() {
        assertEquals("Read User 1 by Email", users.get(0), DAOFactory.getInstance().readUserByEmail(users.get(0).getEmail()));
        assertEquals("Read User 2 by Email", users.get(1), DAOFactory.getInstance().readUserByEmail(users.get(1).getEmail()));
        assertEquals("Read User 3 by Email", users.get(2), DAOFactory.getInstance().readUserByEmail(users.get(2).getEmail()));
        assertEquals("Read User 4 by Email", users.get(3), DAOFactory.getInstance().readUserByEmail(users.get(3).getEmail()));
    }

    @Test
    @Override
    public void testUpdate() {
        assertEquals("User Without Update", users.get(0), DAOFactory.getInstance().readUser(users.get(0).getID()));
        users.get(0).setRolle(gast);
        assertTrue("Update Rolle by User", DAOFactory.getInstance().updateUser(users.get(0)));
        assertEquals("Update User Confirmation", users.get(0), DAOFactory.getInstance().readUser(users.get(0).getID()));
    }

    @Test
    @Override
    public void testRead() {
        assertEquals("Read User 1", users.get(0), DAOFactory.getInstance().readUser(users.get(0).getID()));
        assertEquals("Read User 2", users.get(1), DAOFactory.getInstance().readUser(users.get(1).getID()));
        assertEquals("Read User 3", users.get(2), DAOFactory.getInstance().readUser(users.get(2).getID()));
        assertEquals("Read User 4", users.get(3), DAOFactory.getInstance().readUser(users.get(3).getID()));
    }

    @Test
    @Override
    public void testInsert() {
        User user = new User(15, "TestUser", Typ.FIRMA, "", "", admin, "test5@lustig.at", "12345678", "0664", "", 7423, "");
        users.add(user);
        assertTrue("Insert User", DAOFactory.getInstance().createUser(user) != null);

    }

    @Test
    @Override
    public void testDelete() {
        assertTrue("Delete User 1", DAOFactory.getInstance().deleteUser(users.get(0)));
        assertTrue("Delete User 2", DAOFactory.getInstance().deleteUser(users.get(1)));
        assertTrue("Delete User 3", DAOFactory.getInstance().deleteUser(users.get(2)));
        assertTrue("Delete User 4", DAOFactory.getInstance().deleteUser(users.get(3)));

        users.clear();
    }
}
