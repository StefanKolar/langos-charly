package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import at.htlpinkafeld.langoscharly.utilities.PasswortEncryption;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Kolar
 */
public class UserBean implements BaseBean {

    private LangosCharlyService service;
    private InputStream bild;
    private int userID;
    private String firmenname;
    private Typ typ = Typ.FIRMA;
    private String vorname, nachname;
    private String email;
    private String passwort;
    private String telefonnummer;
    private String adresse;
    private int plz;
    private String ort;
    private Rolle rolle;
    private User changeUser;
    private int rollenID;
    private String neuesPasswort;

    private List<Rolle> rollen;
    private final String cookieUserID = "cUserID";
    private final String cookiePw = "cPw";
    private final int cookieDuration = 2628000; // 1 Monat
    private final String cookiePath = "/";
    private final boolean cookieHttpOnly = true;

    public UserBean() {
    }

    @PostConstruct
    public void init() {
        typ = Typ.FIRMA;

        new Thread(() -> {
            service.preLoad();
        }).start();
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = ((HttpServletRequest) (context.getExternalContext().getRequest()));
        HttpServletResponse response = ((HttpServletResponse) (context.getExternalContext().getResponse()));

        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (request.getParameter("JSESSIONID") != null) {
            Cookie userCookie = new Cookie("JSESSIONID", request.getParameter("JSESSIONID"));
            response.addCookie(userCookie);
        } else {
            String sessionId = session.getId();
            Cookie userCookie = new Cookie("JSESSIONID", sessionId);
            response.addCookie(userCookie);
        }

        
        Cookie[] cookiesArr = request.getCookies();
        if (cookiesArr != null && cookiesArr.length > 0) {
            for (Cookie cookie : cookiesArr) {
                String cName = cookie.getName();
                String cValue = cookie.getValue();

                if (cName.equals(cookieUserID)) {
                    userID = Integer.valueOf(cValue);
                } else if (cName.equals(cookiePw)) {
                    passwort = cValue;
                }
            }
        }
        if (userID > -1 && passwort != null && passwort.length() > 0) {

            try {

                setCookies(userID, passwort);

            } catch (Exception ex) {
            }

            try {
                passwort = passwort.replace("+", " ");
                login(this.service.getUser(userID), passwort);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(UserBean.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        } else {
            passwort = null;
            userID = -1;
        }
    }

    public boolean isFirma() {
        return typ.equals(Typ.FIRMA);
    }

    public boolean isVerein() {
        return typ.equals(Typ.VEREIN);
    }

    public String getNeuesPasswort() {
        return neuesPasswort;
    }

    public void setNeuesPasswort(String neuesPasswort) {
        this.neuesPasswort = neuesPasswort;
    }

    public LangosCharlyService getService() {
        return service;
    }

    public void setService(LangosCharlyService service) {
        this.service = service;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public InputStream getBild() {
        return bild;
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    public String getFirmenname() {
        return firmenname;
    }

    public void setFirmenname(String Firmenname) {
        this.firmenname = Firmenname;
    }

    public Typ getTyp() throws IOException {
        return typ;
    }

    public void setTyp(Typ typ) {
        this.typ = typ;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String Adresse) {
        this.adresse = Adresse;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getUserID() {
        return userID;
    }

    public Rolle getRolle() {
        return rolle;
    }

    public void setRollenID(int rollenID) {
        this.rollenID = rollenID;
    }

    public int getRollenID() {
        return rollenID;
    }

    public List<Rolle> getRollen() {
        rollen = service.getRollen();
        return rollen;
    }

    public String getName(User user) {
        return service.getName(user);
    }

    public String getName(int id) {
        return getName(this.service.getUser(id));
    }

    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {

            return new DefaultStreamedContent(this.service.getCurrentUser().getBild(), "image/jpeg");
        }
    }

    public User getCurrentUser() {
        return this.service.getCurrentUser();
    }

    public boolean isPrivat() {
        if (service.getCurrentUser() != null) {
            return service.getCurrentUser().getTyp().equals(Typ.PRIVAT);
        }
        return false;
    }

    public boolean isDefaultImage() {
        try {
            if (service.getCurrentUser().getBild() != null) {
                return false;
            }
        } catch (Exception ex) {
        }

        return true;
    }

    public void fileUpload(FileUploadEvent event) {
        try {
            bild = event.getFile().getInputstream();

            BufferedImage sourceImage = ImageIO.read(bild);
            Image thumbnail = sourceImage.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
            BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
                    thumbnail.getHeight(null),
                    BufferedImage.TYPE_INT_RGB);
            bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedThumbnail, "jpeg", baos);
            bild = new ByteArrayInputStream(baos.toByteArray());

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Erfolgreich", "Bild wurde erfolgreich hochgeladen"));

            this.service.updateImage(this.service.getCurrentUser(), bild);

        } catch (IOException e) {
            Logger.getLogger(UserBean.class).fatal("Bild nicht möglich hochzuladen: " + e.getMessage(), e);
        }
    }

    public void changePassword() {
        User user = service.getCurrentUser();
        user.setPasswort(passwort);
        this.service.updateUser(user);
    }

    public String register() throws IOException {

        if (typ.equals(Typ.PRIVAT)) {
            firmenname = Typ.PRIVAT.getName();
        }

        byte[] salt = null;
        byte[] hashedPasswort = null;
        try {
            salt = PasswortEncryption.salt();
            hashedPasswort = PasswortEncryption.hash(passwort, salt);

        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UserBean.class
            ).fatal("User konnte nicht erstellt werden");
            System.out.println(ex.toString());
            throw new RuntimeException("User konnte nicht erstellt werden");
        }

        String salt_passwort = PasswortEncryption.convertToString(salt) + ":" + PasswortEncryption.convertToString(hashedPasswort);

        User user = new User(firmenname, typ, vorname, nachname, this.service.getRolle(LangosCharlyService.GAST_ROLLEN_NUMBER), email, salt_passwort, telefonnummer, adresse, plz, ort);

        this.service.createUser(user, true);
        if (service.getCurrentUser() != null) {
            service.getToBase();
            return backToBase();
        } else {
            return null;
        }
    }

    public List<User> getUsers() {
        return service.getUsers();
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    public String updateUser(User user) {
        service.updateUser(user);
        return null;
    }

    public void deleteUser(User user) {
        this.service.deleteUser(user);
    }

    public void login() throws IOException {
        User user = null;
        FacesContext context = FacesContext.getCurrentInstance();
        String hashedPasswort;

        if (email != null) {
            user = service.getUserByEmail(email);
        }
        if (user != null) {
            String salt = user.getPasswort().split(":")[0];
            try {
                hashedPasswort = PasswortEncryption.convertToString(PasswortEncryption.hash(passwort, PasswortEncryption.toByteArray(salt)));
                login(user, hashedPasswort);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
                Logger.getLogger(UserBean.class).log(Level.FATAL, "Not able to Login", ex);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login", "Email ist nicht bekannt"));
        }

    }

    private String login(User user, String hashedPasswort) throws IOException {

        FacesContext context = FacesContext.getCurrentInstance();

        if (Arrays.equals(PasswortEncryption.toByteArray(hashedPasswort), PasswortEncryption.toByteArray(user.getPasswort().split(":")[1]))) {

            this.service.setCurrentUser(user);
            context.getExternalContext().getSessionMap().remove("user");
            context.getExternalContext().getSessionMap().put("user", user);

            setCookies(user.getID(), hashedPasswort);

            userID = user.getID();
            bild = user.getBild();
            firmenname = user.getFirmenname();
            typ = user.getTyp();
            vorname = user.getVorname();
            nachname = user.getNachname();
            rolle = user.getRolle();
            email = user.getEmail();
            telefonnummer = user.getTelefonnummer();
            adresse = user.getAdresse();
            plz = user.getPlz();
            ort = user.getOrt();

            service.getToBase();

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login", "Passwort ungültig"));
        }

        return null;
    }

    public void logout() throws IOException {
        service.setDefaultUser();

        userID = -1;
        bild = null;
        firmenname = null;
        typ = null;
        vorname = null;
        nachname = null;
        rolle = null;
        email = null;
        passwort = null;
        telefonnummer = null;
        adresse = null;
        plz = -1;
        ort = null;

        Cookie[] cookiesArr = ((HttpServletRequest) (FacesContext.getCurrentInstance().getExternalContext().getRequest())).getCookies();
        if (cookiesArr != null && cookiesArr.length > 0) {
            for (Cookie cookie : cookiesArr) {
                String cName = cookie.getName();
                if (cName.equals(cookieUserID) || cName.equals(cookiePw)) {
                    cookie.setMaxAge(0);
                    cookie.setValue("");
                    cookie.setPath("/");
                    ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).addCookie(cookie);
                }
            }
        }

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        service.getToBase();
    }

    public String backToBase() {
        return LangosCharlyService.BASE_URL;
    }

    public void changeUser() {

        changeUser = service.getCurrentUser();

        changeUser.setTyp(typ);
        changeUser.setFirmenname(firmenname);
        changeUser.setVorname(vorname);
        changeUser.setNachname(nachname);
        changeUser.setEmail(email);
        changeUser.setTelefonnummer(telefonnummer);
        changeUser.setAdresse(adresse);
        changeUser.setPlz(plz);
        changeUser.setOrt(ort);

        this.service.updateUser(changeUser);
    }

    public boolean isDeleteable(User user) {

        if (user == null) {
            return true;
        }

        if (user.getRolle().isSuperadmin()) {
            return false;
        }

        return user.getID() != LangosCharlyService.DEFAULT_USER;

    }

    public void passwortVergessen() {

        final int length = 8;
        final String allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
        SecureRandom random = new SecureRandom();
        StringBuilder pass = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            pass.append(allowedChars.charAt(random.nextInt(allowedChars.length())));
        }

        String newPasswort = pass.toString();

        byte[] salt = null;
        byte[] hashedPasswort = null;
        try {
            salt = PasswortEncryption.salt();
            hashedPasswort = PasswortEncryption.hash(newPasswort, salt);

        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UserBean.class
            ).fatal("User konnte passwort nicht zurücksetzten");
        }

        newPasswort = PasswortEncryption.convertToString(salt) + ":" + PasswortEncryption.convertToString(hashedPasswort);

        User user = null;
        if (email != null) {
            user = this.service.getUserByEmail(email);
        }
        if (user != null) {

            String text = service.getInformation(8).getText();
            StringBuilder username = new StringBuilder();

            switch (user.getTyp()) {
                case PRIVAT:
                    username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                    break;
                case FIRMA:
                    username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                    break;
                case VEREIN:
                    username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                    break;
            }

            text = text.replace("USERNAME", username.toString()).replace("PASSWORT", newPasswort);

            MailClient.sendMail(email, "Passwort vergessen", text);
            user.setPasswort(newPasswort);
            service.updateUser(user);

            Logger
                    .getLogger(UserBean.class
                    ).debug("Passwort zur\u00fcckgesetzt von " + email);

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Passwort zurückgesetzt", "Ein neues Passwort wurde an ihre Email geschickt"));
        }
    }

    public List<Typ> getAllTyps() {
        return new LinkedList<>(Arrays.asList(Typ.values()));
    }

    public String passwortAendern() {

        FacesContext context = FacesContext.getCurrentInstance();

        boolean accept;

        String passwortAlt = passwort;
        passwort = service.getCurrentUser().getPasswort();

        String salt = passwort.split(":")[0];
        String hashedPasswort = passwort.split(":")[1];

        try {
            accept = PasswortEncryption.authenticate(passwortAlt, PasswortEncryption.toByteArray(salt), PasswortEncryption.toByteArray(hashedPasswort));

        } catch (Exception ex) {
            Logger.getLogger(UserBean.class
            ).error("Beim Passwort zurücksetzten ist ein Fehler aufgetreten");
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwort zurücksetzten", "Ein Fehler ist beim Passwort zurückseztten aufgetreten"));
            return null;
        }

        if (accept) {

            byte[] saltNeu = null;
            byte[] hashedPasswortNeu = null;
            try {
                saltNeu = PasswortEncryption.salt();
                hashedPasswortNeu = PasswortEncryption.hash(neuesPasswort, saltNeu);

            } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
                Logger.getLogger(UserBean.class).fatal("Passwort konnte nicht geändert werden");
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwort zurücksetzten", "Ein Fehler ist beim Passwort zurückseztten aufgetreten"));
            }

            String salt_passwort = PasswortEncryption.convertToString(saltNeu) + ":" + PasswortEncryption.convertToString(hashedPasswortNeu);

            passwort = salt_passwort;

            changeUser = service.getCurrentUser();

            changeUser.setPasswort(passwort);

            service.updateUser(changeUser);

            try {

                setCookies(changeUser.getID(), PasswortEncryption.convertToString(hashedPasswortNeu));

            } catch (Exception ex) {

            }

            passwort = neuesPasswort;

            return "meinUser.xhtml?faces-redirect=true";

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Passwort zurücksetzten", "Altes Passwort stimmt nicht überein"));
        }

        return null;
    }

    public void setCookies(int userID, String passwort) throws UnsupportedEncodingException {
        Cookie uID, pw;

        uID = new Cookie(cookieUserID, URLEncoder.encode(String.valueOf(userID), "UTF-8"));
        pw = new Cookie(cookiePw, URLEncoder.encode(passwort, "UTF-8"));

        uID.setMaxAge(cookieDuration);
        pw.setMaxAge(cookieDuration);

        uID.setPath(cookiePath);
        pw.setPath(cookiePath);

        uID.setHttpOnly(cookieHttpOnly);
        pw.setHttpOnly(cookieHttpOnly);

        ((HttpServletResponse) (FacesContext.getCurrentInstance().getExternalContext().getResponse())).addCookie(uID);
        ((HttpServletResponse) (FacesContext.getCurrentInstance().getExternalContext().getResponse())).addCookie(pw);

    }

}
