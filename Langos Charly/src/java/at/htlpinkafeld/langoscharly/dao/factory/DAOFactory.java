package at.htlpinkafeld.langoscharly.dao.factory;

import at.htlpinkafeld.langoscharly.dao.interfaces.BestellDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.BestellpositionDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.FestBuchenDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.FestLangosDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.FestSpeisenDAO;

import at.htlpinkafeld.langoscharly.dao.interfaces.HauptspeisenDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.ImageDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.InformationenDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.LangosDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.RollenDAO;
import at.htlpinkafeld.langoscharly.dao.interfaces.UserDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.BestellpositionJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.BestellungJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.FestBuchenJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.FestLangosJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.FestSpeisenJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.HauptspeisenJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.ImageJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.InformationJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.LangosJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.RollenJdbcDAO;
import at.htlpinkafeld.langoscharly.dao.jdcb.UserJdbcDAO;
import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import at.htlpinkafeld.langoscharly.pojo.FestSpeisen;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import at.htlpinkafeld.langoscharly.pojo.Image;
import at.htlpinkafeld.langoscharly.pojo.Information;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.pojo.Rolle;

import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.InputStream;

import java.util.List;

/**
 *
 * @author Kolar
 */
public final class DAOFactory {

    private static volatile DAOFactory instance;

    private final UserDAO userDAO = new UserJdbcDAO();
    private final RollenDAO rollenDAO = new RollenJdbcDAO();
    private final LangosDAO langosDAO = new LangosJdbcDAO();
    private final HauptspeisenDAO hauptspeisenDAO = new HauptspeisenJdbcDAO();
    private final BestellDAO bestellungDAO = new BestellungJdbcDAO();
    private final BestellpositionDAO bestellpositionDAO = new BestellpositionJdbcDAO();
    private final ImageDAO imageDAO = new ImageJdbcDAO();
    private final FestBuchenDAO buchungDAO = new FestBuchenJdbcDAO();
    private final FestLangosDAO festLangosDAO = new FestLangosJdbcDAO();
    private final FestSpeisenDAO festSpeisenDAO = new FestSpeisenJdbcDAO();
    private final InformationenDAO informationenDAO = new InformationJdbcDAO();

    private DAOFactory() {
    }

    public static synchronized DAOFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }

    /*
    
    
    Rollen
    
    
     */
    public Rolle createRolle(Rolle rolle) throws UserException {
        if (rollenDAO.create(rolle)) {
            return rolle;
        }
        return null;
    }

    public Rolle readRolle(int id) throws UserException {
        Rolle rolle = rollenDAO.read(id);
        return rolle;
    }

    public List<Rolle> readAllRollen() {
        List<Rolle> rollen = rollenDAO.readAll();

        return rollen;
    }

    public boolean updateRolle(Rolle rolle) {
        boolean retVal = rollenDAO.update(rolle);
        return retVal;

    }

    public boolean deleteRolle(Rolle rolle) {
        return rollenDAO.delete(rolle);
    }

    /*
    
    
    Users
    
    
     */
    public User createUser(User user) {
        userDAO.create(user);

        return user;
    }

    public User readUser(int id) throws UserException {
        User user = userDAO.read(id);
        user.setRolle(readRolle(user.getRolle().getID()));
        return user;
    }

    public List<User> readAllUser() throws UserException {

        List<User> users = userDAO.readAll();

        for (User user : users) {

            user.setRolle(readRolle(user.getRolle().getID()));
        }

        return users;

    }

    public boolean updateUser(User user) {
        boolean retVal = userDAO.update(user);

        return retVal;
    }

    public boolean deleteUser(User user) {
        return userDAO.delete(user);
    }

    public User readUserByEmail(String email) throws UserException {
        User user = userDAO.readUserByEmail(email);
        if (user != null) {
            user.setRolle(readRolle(user.getRolle().getID()));
            return user;
        }
        return null;
    }

    public InputStream readUserImageByID(int id) {
        return userDAO.readUserImageByID(id);
    }

    public boolean updateImage(User user, InputStream bild) {
        return userDAO.updateImage(user.getID(), bild);
    }

    /*
    
    
    Langos
    
    
     */
    public Langos createLangos(Langos langos) {
        if (langosDAO.create(langos)) {
            return langos;
        }
        return null;
    }

    public Langos readLangos(int id) {
        Langos rolle = langosDAO.read(id);
        return rolle;
    }

    public List<Langos> readAllLangos() {
        List<Langos> langos = langosDAO.readAll();

        return langos;
    }

    public boolean updateLangos(Langos langos) {
        boolean retVal = langosDAO.update(langos);
        return retVal;

    }

    public boolean deleteLangos(Langos langos) {
        return langosDAO.delete(langos);
    }

    public InputStream readLangosImageByID(int id) {
        return langosDAO.readLangosImageByID(id);
    }

    public boolean updateImage(Langos langos, InputStream bild) {
        return langosDAO.updateImage(langos.getID(), bild);
    }

    public int readLangosMengeByID(int id) {
        return langosDAO.readMengeByID(id);
    }

    public boolean updateMenge(int langosID, int menge) {
        return langosDAO.updateMenge(langosID, menge);
    }

    /*
    
    
    Hauptspeisen
    
    
     */
    public Hauptspeise createHauptspeise(Hauptspeise hauptspeise) {
        if (hauptspeisenDAO.create(hauptspeise)) {
            return hauptspeise;
        }
        return null;
    }

    public Hauptspeise readHauptspeise(int id) {
        Hauptspeise rolle = hauptspeisenDAO.read(id);
        return rolle;
    }

    public List<Hauptspeise> readAllHauptspeisen() {
        List<Hauptspeise> hauptspeise = hauptspeisenDAO.readAll();

        return hauptspeise;
    }

    public boolean updateHauptspeise(Hauptspeise hauptspeise) {
        boolean retVal = hauptspeisenDAO.update(hauptspeise);
        return retVal;

    }

    public boolean deleteHauptspeise(Hauptspeise hauptspeise) {
        return hauptspeisenDAO.delete(hauptspeise);
    }

    public InputStream readHauptspeisenImageByID(int id) {
        return hauptspeisenDAO.readHauptspeisenImageByID(id);
    }

    public boolean updateImage(Hauptspeise hauptspeise, InputStream bild) {
        return hauptspeisenDAO.updateImage(hauptspeise.getID(), bild);
    }

    /*
    
    
    Bestellung
    
    
     */
    public Bestellung createBestellung(Bestellung bestellung) {
        if (bestellungDAO.create(bestellung)) {
            return bestellung;
        }
        return null;
    }

    public Bestellung readBestellung(int id) {
        Bestellung bestellung = bestellungDAO.read(id);
        return bestellung;
    }

    public List<Bestellung> readAllBestellung() {
        List<Bestellung> bestellungen = bestellungDAO.readAll();

        return bestellungen;
    }

    public List<Bestellung> readBestellungenByUser(User user) {
        List<Bestellung> bestellungen = bestellungDAO.readBestellungByUser(user);
        return bestellungen;
    }

    public boolean updateBestellung(Bestellung bestellung) {
        boolean retVal = bestellungDAO.update(bestellung);
        return retVal;

    }

    public boolean deleteBestellung(Bestellung bestellung) {
        return bestellungDAO.delete(bestellung);
    }

    /*
    
    
    Bestellposition
    
    
     */
    public Bestellposition createBestellposition(Bestellposition bestellposition) {
        if (bestellpositionDAO.create(bestellposition)) {
            return bestellposition;
        }
        return null;
    }

    public Bestellposition readBestellposition(int id) {
        return bestellpositionDAO.read(id);
    }

    public List<Bestellposition> readAllBestellposition() {
        return bestellpositionDAO.readAll();
    }

    public List<Bestellposition> readAllBestellpositionByBestellID(int id) {
        List<Bestellposition> bestellpositionen = bestellpositionDAO.readBestellpositionenByBestellID(id);

        return bestellpositionen;
    }

    public boolean updateBestellposition(Bestellposition bestellposition) {
        boolean retVal = bestellpositionDAO.update(bestellposition);
        return retVal;

    }

    public boolean deleteBestellposition(Bestellposition bestellposition) {
        return bestellpositionDAO.delete(bestellposition);
    }

    /*
    
    
    Bilder
    
    
     */
    public Image createImage(Image image) {
        if (imageDAO.create(image)) {
            return image;
        }
        return null;
    }

    public Image readImage(int id) {
        return imageDAO.read(id);
    }

    public List<Image> readAllImage() {
        return imageDAO.readAll();
    }

    public boolean updateImage(Image image) {
        return imageDAO.update(image);

    }

    public boolean deleteImage(Image image) {
        return imageDAO.delete(image);
    }

    public boolean updateImage(Image image, InputStream bild) {
        return imageDAO.updateImage(image.getID(), bild);
    }

    public InputStream readImageBildByID(int id) {
        return imageDAO.readImageBildByID(id);
    }

    /*
    
    
    Fest buchen
    

     */
    public Fest createBuchung(Fest buchung) {

        if (buchungDAO.create(buchung)) {

            return buchung;
        }
        return null;
    }

    public boolean deleteBuchung(Fest buchung) {
        return buchungDAO.delete(buchung);
    }

    public Fest readBuchung(int id) {
        Fest buchung = buchungDAO.read(id);
        return buchung;
    }

    public List<Fest> readAllBuchung() {
        List<Fest> buchungen;
        buchungen = buchungDAO.readAll();
        return buchungen;
    }

    public boolean updateBuchung(Fest buchung) {
        boolean retVal = buchungDAO.update(buchung);
        return retVal;
    }

    /*
    
        FestLangos
    
     */
    public FestLangos createFestLangos(FestLangos fl) {
        if (festLangosDAO.create(fl)) {
            return fl;
        }
        return null;
    }

    public boolean deleteFestLangos(FestLangos fl) {
        return false;
    }

    public List<FestLangos> readFestLangosByFestID(int id) {
        return festLangosDAO.readFestLangosByFestID(id);
    }

    public List<FestLangos> readAllFestLangos() {
        return festLangosDAO.readAll();
    }

    public boolean updateFestLangos(FestLangos fl) {
        return festLangosDAO.update(fl);
    }

    /*
    
        FestSpeisen
    
     */
    public FestSpeisen createFestSpeisen(FestSpeisen fs) {
        if (festSpeisenDAO.create(fs)) {
            return fs;
        }
        return null;
    }

    public boolean deleteFestSpeisen(FestSpeisen fs) {
        return false;
    }

    public List<FestSpeisen> readFestSpeisenByFestID(int id) {
        return festSpeisenDAO.readFestSpeisenByFestID(id);
    }

    public List<FestSpeisen> readAllFestSpeisen() {
        return festSpeisenDAO.readAll();
    }

    public boolean updateFestSpeisen(FestSpeisen fs) {
        return festSpeisenDAO.update(fs);
    }

    /*
    
    
    Informationen
    
    
     */
    public Information createInformation(Information information) throws UserException {
        if (informationenDAO.create(information)) {
            return information;
        }
        return null;
    }

    public Information readInformation(int id) throws UserException {
        return informationenDAO.read(id);
    }

    public List<Information> readAllInformationen() {
        return informationenDAO.readAll();
    }

    public boolean updateInformation(Information information) {
        return informationenDAO.update(information);
    }

    public boolean deleteInformation(Information information) {
        return informationenDAO.delete(information);
    }

}
