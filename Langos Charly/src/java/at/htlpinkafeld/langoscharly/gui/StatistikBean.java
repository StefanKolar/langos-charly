package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import at.htlpinkafeld.langoscharly.pojo.FestSpeisen;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author Kolar
 */
public class StatistikBean implements BaseBean {

    private LangosCharlyService service;

    private boolean typ = false; //Bestellung = true; Fest = false
    private boolean selectAll = false;
    private List<Fest> selectedFeste;
    private List<Bestellung> selectedBestellungen;

    public StatistikBean() {
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public boolean isTyp() {
        return typ;
    }

    public void setTyp(boolean typ) {
        this.typ = typ;
        selectAll = false;
        selectedBestellungen = null;
        selectedFeste = null;
    }

    public List<Fest> getFeste() {
        try {
            List<Fest> helpList = new LinkedList<>();
            for(Fest fest : service.getFeste())
                if(fest.isAbgeschlossen()){
                    helpList.add(fest);
                }
            return helpList;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Keine Daten zum anzeigen vorhanden"));
        }
        return null;
    }

    public List<Bestellung> getBestellungen() {
        try {
            List<Bestellung> helpList = new LinkedList<>();
            for(Bestellung bestellung : service.getBestellungen())
                if(bestellung.isBestaetigt()){
                    helpList.add(bestellung);
                }
            return helpList;
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Keine Daten zum anzeigen vorhanden"));
        }
        return null;
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        this.selectAll = selectAll;
    }

    public void onSelectAll() {
        if (selectAll) {
            if (typ) {
                selectedBestellungen = getBestellungen();
            } else {
                selectedFeste = getFeste();
            }
        } else {
            selectedFeste = null;
            selectedBestellungen = null;
        }
    }

    public List<Fest> getSelectedFeste() {
        return selectedFeste;
    }

    public void setSelectedFeste(List<Fest> selectedFeste) {
        this.selectedFeste = selectedFeste;
    }

    public List<Bestellung> getSelectedBestellungen() {
        return selectedBestellungen;
    }

    public void setSelectedBestellungen(List<Bestellung> selectedBestellungen) {
        this.selectedBestellungen = selectedBestellungen;
    }

    public PieChartModel getModel() {
        PieChartModel model = new PieChartModel();
        Map<String, Number> map = new HashMap<>();

        if (typ) {

            if (selectedBestellungen == null) {
                return new PieChartModel();
            }

            int idx = 0;
            for (Bestellung b : selectedBestellungen) {
                if (idx == 0) {
                    map = getMapOutOfBestellung(b);
                    idx++;
                } else {
                    map = concatMaps(map, getMapOutOfBestellung(b));
                }
            }
            model.setData(map);

        } else {

            if (selectedFeste == null) {
                return new PieChartModel();
            }

            int idx = 0;
            for (Fest f : selectedFeste) {
                if (idx == 0) {
                    map = getMapOutOfFest(f);
                    idx++;
                } else {
                    map = concatMaps(map, getMapOutOfFest(f));
                }
            }
            model.setData(map);

        }

        model.setLegendPosition("w");
        model.setShowDataLabels(true);
        model.setDiameter(150);
        model.setShadow(false);
        model.setLegendCols(3);

        return model;
    }

    private Map<String, Number> getMapOutOfBestellung(Bestellung b) {
        Map<String, Number> map = new HashMap<>();

        for (Bestellposition bp : service.getBestellpositionenByBestellID(b.getID())) {
            map.put(service.getLangos(bp.getLangosID()).getName(), (Number) (bp.getMenge()));
        }

        return map;
    }

    private Map<String, Number> getMapOutOfFest(Fest f) {
        Map<String, Number> map = new HashMap<>();

        for (FestSpeisen speise : service.getFestSpeisenByFestID(f.getID())) {
            map.put(service.getHauptspeise(speise.getHauptspeiseID()).getName(), speise.getMenge());
        }

        for (FestLangos langos : service.getFestLangosByFestID(f.getID())) {
            map.put(service.getLangos(langos.getLangosID()).getName(), (Number) (langos.getMenge()));
        }

        return map;
    }

    private Map<String, Number> concatMaps(Map<String, Number> m1, Map<String, Number> m2) {
        Map<String, Number> map = new HashMap<>(m1);

        m2.forEach((key, value) -> map.merge(key, value, (v1, v2) -> (Number) (v1.intValue() + v2.intValue())));

        return map;
    }

}
