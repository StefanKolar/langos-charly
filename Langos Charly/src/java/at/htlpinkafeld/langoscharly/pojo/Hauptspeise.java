package at.htlpinkafeld.langoscharly.pojo;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import java.io.InputStream;
import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class Hauptspeise implements Identifiable {

    private int hauptspeienID;
    private String name;
    private String beschreibung;
    private String allergene;
    private InputStream bild;
    private boolean verfuegbar;
    private String bemerkung;

    public Hauptspeise(String name, String beschreibung, String allergene, boolean verfuegbar, String bemerkung) {
        this.name = name;
        this.beschreibung = beschreibung;
        this.allergene = allergene;
        this.verfuegbar = verfuegbar;
        this.bemerkung = bemerkung;
    }

    public Hauptspeise(int hauptspeienID, String name, String beschreibung, String allergene, boolean verfuegbar, String bemerkung) {
        this(name, beschreibung, allergene, verfuegbar, bemerkung);
        this.hauptspeienID = hauptspeienID;
    }

    @Override
    public int getID() {
        return hauptspeienID;
    }

    @Override
    public void setID(int hauptspeienID) {
        this.hauptspeienID = hauptspeienID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getAllergene() {
        return allergene;
    }

    public void setAllergene(String allergene) {
        this.allergene = allergene;
    }

    public InputStream getBild() {
        return DAOFactory.getInstance().readHauptspeisenImageByID(hauptspeienID);
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }
    
    public boolean isVerfuegbar() {
        return verfuegbar;
    }

    public void setVerfuegbar(boolean verfuegbar) {
        this.verfuegbar = verfuegbar;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.hauptspeienID;
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.beschreibung);
        hash = 41 * hash + Objects.hashCode(this.allergene);
        hash = 41 * hash + Objects.hashCode(this.bild);
        hash = 41 * hash + (this.verfuegbar ? 1 : 0);
        hash = 41 * hash + Objects.hashCode(this.bemerkung);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hauptspeise other = (Hauptspeise) obj;
        if (this.hauptspeienID != other.hauptspeienID) {
            return false;
        }
        if (this.verfuegbar != other.verfuegbar) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.beschreibung, other.beschreibung)) {
            return false;
        }
        if (!Objects.equals(this.allergene, other.allergene)) {
            return false;
        }
        if (!Objects.equals(this.bild, other.bild)) {
            return false;
        }
        if (!Objects.equals(this.bemerkung, other.bemerkung)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Langos{" + "hauptspeienID=" + hauptspeienID + ", name=" + name + ", beschreibung=" + beschreibung + ", allergene=" + allergene + ", bild=" + bild + ", verfuegbar=" + verfuegbar + ", bemerkung=" + bemerkung + '}';
    }
    
    
}

