package at.htlpinkafeld.langoscharly.service;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.pojo.FestSpeisen;
import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import at.htlpinkafeld.langoscharly.pojo.Image;
import at.htlpinkafeld.langoscharly.pojo.Information;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.activation.DataHandler;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kolar
 */
public final class LangosCharlyService implements Serializable {

    private List<User> users = new LinkedList<>();
    private List<Rolle> rollen = new LinkedList<>();
    private List<Langos> langos = new LinkedList<>();
    private List<Hauptspeise> hauptspeisen = new LinkedList<>();
    private List<Bestellung> bestellungen = new LinkedList<>();
    private List<Bestellposition> bestellpositionen = new LinkedList<>();
    private List<Image> bilder = new LinkedList<>();
    private List<Fest> buchungen = new LinkedList<>();
    private List<FestLangos> festLangos = new LinkedList<>();
    private List<FestSpeisen> festSpeisen = new LinkedList<>();
    private List<Information> informationen = new LinkedList<>();

    private User currentUser;

    public static final String GAST = "Gast";
    public static final int GAST_ROLLEN_NUMBER = 6;
    public static final int DEFAULT_USER = 1;
    public static final String BASE_URL = "/";
    public static final String BASE_EMAIL = "langos-charly@outlook.com";
    private static final Logger LOGGER = Logger.getLogger(LangosCharlyService.class);

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void getToBase() throws IOException {
        if (!FacesContext.getCurrentInstance().getExternalContext().isResponseCommitted()) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).encodeRedirectURL(BASE_URL));
        }
    }

    public LangosCharlyService() throws IOException {
        setDefaultUser();
    }

    /*
    User
     */
    public List<User> getUsers() {
        if (users.isEmpty()) {
            users = DAOFactory.getInstance().readAllUser();
        }
        return users;
    }

    public User getUser(int id) {
        return DAOFactory.getInstance().readUser(id);
    }

    public void createUser(User u, boolean register) {
        User user = DAOFactory.getInstance().createUser(u);

        if (users.isEmpty()) {
            users = DAOFactory.getInstance().readAllUser();
        } else if (user != null) {
            users.add(user);
        }

        if (register) {
            currentUser = user;
        }
    }

    public void deleteUser(User user) {
        if (DAOFactory.getInstance().deleteUser(user)) {
            this.users.remove(user);
        }
    }

    public void updateUser(User user) {
        DAOFactory.getInstance().updateUser(user);
    }

    public User getUserByEmail(String email) {
        return DAOFactory.getInstance().readUserByEmail(email);
    }

    public boolean updateImage(User user, InputStream bild) {
        return DAOFactory.getInstance().updateImage(user, bild);
    }

    /*
    Rolle
     */
    public List<Rolle> getRollen() {
        if (rollen.isEmpty()) {
            rollen = DAOFactory.getInstance().readAllRollen();
        }
        return rollen;
    }

    public Rolle getRolle(int id) {
        return DAOFactory.getInstance().readRolle(id);
    }

    public void createRolle(Rolle r) {
        Rolle rolle = DAOFactory.getInstance().createRolle(r);
        if (rollen.isEmpty()) {
            rollen = DAOFactory.getInstance().readAllRollen();
        } else if (rolle != null) {
            this.rollen.add(rolle);
        }
    }

    public void deleteRolle(Rolle rolle) {
        if (DAOFactory.getInstance().deleteRolle(rolle)) {
            rollen.remove(rolle);
        }
    }

    public void updateRolle(Rolle rolle) {
        DAOFactory.getInstance().updateRolle(rolle);
    }
    
    public Rolle getRolleByName(String name) {
        for (Rolle rolle : getRollen()) {
            if (rolle.getRolle().equals(name)) {
                return rolle;
            }
        }
        return null;
    }

    /*
    Langos
     */
    public List<Langos> getLangos() {
        if (langos.isEmpty()) {
            langos = DAOFactory.getInstance().readAllLangos();
        }
        return langos;
    }

    public Langos getLangos(int id) {
        return DAOFactory.getInstance().readLangos(id);
    }

    public void createLangos(Langos l) {
        Langos langos = DAOFactory.getInstance().createLangos(l);
        if (this.langos.isEmpty()) {
            this.langos = DAOFactory.getInstance().readAllLangos();
        } else if (langos != null) {
            this.langos.add(langos);
        }
    }

    public void deleteLangos(Langos langos) {
        if (DAOFactory.getInstance().deleteLangos(langos)) {
            this.langos.remove(langos);
        }
    }

    public void updateLangos(Langos langos) {
        DAOFactory.getInstance().updateLangos(langos);
    }

    public List<Langos> getVerfuegbareLangos() {
        List<Langos> langosList = new LinkedList<>();

        for (Langos l : getLangos()) {
            if (l.isVerfuegbar()) {
                langosList.add(l);
            }
        }

        return langosList;
    }

    public boolean updateImage(Langos langos, InputStream bild) {
        return DAOFactory.getInstance().updateImage(langos, bild);
    }

    public boolean updateMenge(int langosID, int menge) {
        return DAOFactory.getInstance().updateMenge(langosID, menge);
    }

    /*
    Hauptspeise
     */
    public List<Hauptspeise> getHauptspeisen() {
        if (hauptspeisen.isEmpty()) {
            hauptspeisen = DAOFactory.getInstance().readAllHauptspeisen();
        }
        return hauptspeisen;
    }

    public Hauptspeise getHauptspeise(int id) {
        return DAOFactory.getInstance().readHauptspeise(id);
    }

    public void createHauptspeise(Hauptspeise hs) {
        Hauptspeise hauptspeise = DAOFactory.getInstance().createHauptspeise(hs);
        if (this.hauptspeisen.isEmpty()) {
            this.hauptspeisen = DAOFactory.getInstance().readAllHauptspeisen();
        } else if (hauptspeise != null) {
            this.hauptspeisen.add(hauptspeise);
        }
    }

    public void deleteHauptspeise(Hauptspeise hauptspeise) {
        if (DAOFactory.getInstance().deleteHauptspeise(hauptspeise)) {
            hauptspeisen.remove(hauptspeise);
        }
    }

    public void updateHauptspeise(Hauptspeise hauptspeise) {
        DAOFactory.getInstance().updateHauptspeise(hauptspeise);
    }

    public List<Hauptspeise> getVerfuegbareHauptspeisen() {
        List<Hauptspeise> hauptspeisenList = new LinkedList<>();

        for (Hauptspeise h : getHauptspeisen()) {
            if (h.isVerfuegbar()) {
                hauptspeisenList.add(h);
            }
        }

        return hauptspeisenList;
    }

    public boolean updateImage(Hauptspeise hauptspeise, InputStream bild) {
        return DAOFactory.getInstance().updateImage(hauptspeise, bild);
    }

    /*
    Bestellung
     */
    public List<Bestellung> getBestellungen() {
        if (bestellungen.isEmpty()) {
            bestellungen = DAOFactory.getInstance().readAllBestellung();
        }
        return bestellungen;
    }

    public List<Bestellung> getBestellungenByUser(User user) {
        return DAOFactory.getInstance().readBestellungenByUser(user);
    }

    public Bestellung getBestellung(int id) {
        return DAOFactory.getInstance().readBestellung(id);
    }

    public Bestellung createBestellung(Bestellung b) {
        Bestellung bestellung = DAOFactory.getInstance().createBestellung(b);
        if (this.bestellungen.isEmpty()) {
            this.bestellungen = DAOFactory.getInstance().readAllBestellung();
        } else if (bestellung != null) {
            this.bestellungen.add(bestellung);
        }

        return bestellung;
    }

    public void deleteBestellung(Bestellung bestellung) {
        if (DAOFactory.getInstance().deleteBestellung(bestellung)) {
            this.bestellungen.remove(bestellung);
        }
    }

    public void updateBestellung(Bestellung bestellung) {
        DAOFactory.getInstance().updateBestellung(bestellung);
    }

    /*
    Bestellposition
     */
    public List<Bestellposition> getBestellpositionenByBestellID(int id) {
        return DAOFactory.getInstance().readAllBestellpositionByBestellID(id);
    }

    public List<Bestellposition> getBestellpositionen() {
        if (bestellpositionen.isEmpty()) {
            bestellpositionen = DAOFactory.getInstance().readAllBestellposition();
        }
        return bestellpositionen;
    }

    public Bestellposition getBestellposition(int id) {
        return DAOFactory.getInstance().readBestellposition(id);
    }

    public Bestellposition createBestellposition(Bestellposition bestellposition) {
        Bestellposition bestellp = DAOFactory.getInstance().createBestellposition(bestellposition);
        if (bestellpositionen.isEmpty()) {
            bestellpositionen = DAOFactory.getInstance().readAllBestellposition();
        } else if (bestellp != null) {
            bestellpositionen.add(bestellp);
        }
        return bestellp;
    }

    public void deleteBestellposition(Bestellposition bestellposition) {
        if (DAOFactory.getInstance().deleteBestellposition(bestellposition)) {
            this.bestellpositionen.remove(bestellposition);
        }
    }

    public void updateBestellposition(Bestellposition bestellposition) {
        DAOFactory.getInstance().updateBestellposition(bestellposition);
    }

    /*
    Bilder
     */
    public List<Image> getBilder() {
        if (bilder.isEmpty()) {
            bilder = DAOFactory.getInstance().readAllImage();
        }
        return bilder;
    }

    public Image getBild(int id) {
        return DAOFactory.getInstance().readImage(id);
    }

    public void createImage(Image i) {
        Image image = DAOFactory.getInstance().createImage(i);

        if (bilder.isEmpty()) {
            bilder = DAOFactory.getInstance().readAllImage();
        } else if (image != null) {
            this.bilder.add(image);
        }
    }

    public void deleteImage(Image i) {
        if (DAOFactory.getInstance().deleteImage(i)) {
            this.bilder.remove(i);
        }
    }

    public void updateImage(Image image) {
        DAOFactory.getInstance().updateImage(image);
    }

    public void updateImage(Image image, InputStream bild) {
        DAOFactory.getInstance().updateImage(image, bild);
    }

    /*
    FestBuchen
     */
    public List<Fest> getFeste() {
        if (buchungen.isEmpty()) {
            buchungen = DAOFactory.getInstance().readAllBuchung();
        }
        return buchungen;
    }

    public Fest getFest(int id) {
        return DAOFactory.getInstance().readBuchung(id);
    }

    public Fest createFest(Fest f) {
        Fest buchung = DAOFactory.getInstance().createBuchung(f);

        if (buchungen.isEmpty()) {
            buchungen = DAOFactory.getInstance().readAllBuchung();
        } else if (buchung != null) {
            this.buchungen.add(buchung);
        }
        return buchung;
    }

    public void deleteFest(Fest buchung) {
        if (DAOFactory.getInstance().deleteBuchung(buchung)) {
            this.buchungen.remove(buchung);
        }
    }

    public void updateFest(Fest buchung) {
        DAOFactory.getInstance().updateBuchung(buchung);
    }

    /*
    FestLangos
     */
    public List<FestLangos> getFestLangos() {
        if (festLangos.isEmpty()) {
            this.festLangos = DAOFactory.getInstance().readAllFestLangos();
        }
        return this.festLangos;
    }

    public List<FestLangos> getFestLangosByFestID(int id) {
        return DAOFactory.getInstance().readFestLangosByFestID(id);
    }

    public FestLangos createFestLangos(FestLangos f) {
        FestLangos festL = DAOFactory.getInstance().createFestLangos(f);

        if (festLangos.isEmpty()) {
            festLangos = DAOFactory.getInstance().readAllFestLangos();
        } else if (festL != null) {
            this.festLangos.add(festL);
        }
        return festL;
    }

    public void deleteFestLangos(FestLangos f) {
        if (DAOFactory.getInstance().deleteFestLangos(f)) {
            this.festLangos.remove(f);
        }
    }

    public void updateFestLangos(FestLangos b) {
        DAOFactory.getInstance().updateFestLangos(b);
    }

    /*
    FestSpeisen
     */
    public List<FestSpeisen> getFestSpeisen() {
        if (festSpeisen.isEmpty()) {
            this.festSpeisen = DAOFactory.getInstance().readAllFestSpeisen();
        }
        return this.festSpeisen;
    }

    public List<FestSpeisen> getFestSpeisenByFestID(int id) {
        return DAOFactory.getInstance().readFestSpeisenByFestID(id);
    }

    public FestSpeisen createFestSpeisen(FestSpeisen f) {
        FestSpeisen festL = DAOFactory.getInstance().createFestSpeisen(f);

        if (festSpeisen.isEmpty()) {
            festSpeisen = DAOFactory.getInstance().readAllFestSpeisen();
        } else if (festL != null) {
            this.festSpeisen.add(festL);
        }

        return festL;
    }

    public void deleteFestSpeisen(FestSpeisen f) {
        if (DAOFactory.getInstance().deleteFestSpeisen(f)) {
            this.festSpeisen.remove(f);
        }
    }

    public void updateFesSpeisen(FestSpeisen b) {
        DAOFactory.getInstance().updateFestSpeisen(b);
    }

    /*
    Information
     */
    public List<Information> getInformationen() {
        if (informationen.isEmpty()) {
            informationen = DAOFactory.getInstance().readAllInformationen();
        }
        return informationen;
    }

    public Information getInformation(int id) {
        return DAOFactory.getInstance().readInformation(id);
    }

    public void createInformation(Information i) {
        Information information = DAOFactory.getInstance().createInformation(i);
        if (informationen.isEmpty()) {
            informationen = DAOFactory.getInstance().readAllInformationen();
        } else if (information != null) {
            this.informationen.add(information);
        }
    }

    public void deleteInformation(Information information) {
        if (DAOFactory.getInstance().deleteInformation(information)) {
            informationen.remove(information);
        }
    }

    public void updateInformation(Information information) {
        DAOFactory.getInstance().updateInformation(information);
    }

    /*
    
    
    Sonstiges
    
    
     */
    public void addEvent(Bestellung bestellung) {

        new Thread(() -> {
            if (bestellung.isBestaetigt()) {

                try {

                    StringBuilder langosBuilder = new StringBuilder();

                    for (Bestellposition bp : getBestellpositionenByBestellID(bestellung.getID())) {
                        langosBuilder.append(getLangos(bp.getLangosID()).getName()).append(":   ").append(bp.getMenge()).append("\\n");
                    }

                    //Bestätigung für Langos Charly mit Event
                    StringBuilder description = new StringBuilder();

                    User user = getUser(bestellung.getUserID());

                    description.append("Kontaktperson:\nVorname: ").append(user.getVorname()).append(
                            "\nNachname: ").append(user.getNachname()).append(
                            "\nTelefon: ").append(user.getTelefonnummer()).append(
                            "\nEmail: ").append(user.getEmail()).append("\n\n");

                    description.append(langosBuilder.toString());

                    MailClient.sendMail(BASE_EMAIL, "Bestellung: " + bestellung.getOrt(), "Automatisch generierte Email\n\nMit freundlichen Grüßen\nIhr IT-Team",
                            addEvent(bestellung.getDatum().atStartOfDay(), bestellung.getDatum().atStartOfDay().plusHours(22), bestellung.getOrt() + " " + bestellung.getAdresse(), description.toString(), bestellung.getOrt()));

                    
                    
                    
                    //Bestellbestätigung für Kunden
                    langosBuilder = new StringBuilder();

                    langosBuilder.append("Lieferdatum: ").append(bestellung.getDatum().format(DateTimeFormatter.ofPattern("dd LLLL yyyy"))).append("\n");

                    for (Bestellposition bp : getBestellpositionenByBestellID(bestellung.getID())) {
                        langosBuilder.append(getLangos(bp.getLangosID()).getName()).append(":   ").append(bp.getMenge()).append("\n");
                    }

                    String text = getInformation(6).getText();
                    StringBuilder username = new StringBuilder();

                    switch (user.getTyp()) {
                        case PRIVAT:
                            username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                            break;
                        case FIRMA:
                            username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                            break;
                        case VEREIN:
                            username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                            break;
                    }

                    text = text.replace("USERNAME", username.toString()).replace("DETAILS", langosBuilder.toString());
                    MailClient.sendMail(getUser(bestellung.getUserID()).getEmail(), "Bestellbestätigung", text);

                } catch (MessagingException | IOException e) {
                    LOGGER.warn("Event konnte nicht hinzugefügt werden", e);
                    throw new UserException(FacesMessage.SEVERITY_WARN, e);

                }
            }
        }).start();
    }

    public void addEvent(Fest fest) {

        new Thread(() -> {
            if (fest.isBestaetigt()) {

                try {

                    //Bestellbestätigung für Kunden
                    User user = getUser(fest.getUserID());

                    if (user != null) {

                        String text = getInformation(6).getText();
                        StringBuilder username = new StringBuilder();

                        switch (user.getTyp()) {
                            case PRIVAT:
                                username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                                break;
                            case FIRMA:
                                username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                                break;
                            case VEREIN:
                                username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                                break;
                        }

                        text = text.replace("USERNAME", username.toString()).replace("DETAILS", fest.getAnmerkungen());

                        MailClient.sendMail(fest.getEmail(), "Bestellbestätigung", text);

                        //Email to Langos Charly
                        StringBuilder description = new StringBuilder();

                        description.append("Kontaktperson:\nVorname: ").append(fest.getVorname()).append(
                                "\nNachname: ").append(fest.getNachname()).append(
                                "\nTelefon: ").append(fest.getTelefonnummer()).append(
                                "\nEmail: ").append(fest.getEmail()).append("\n\n");

                        description.append("Festinformationen:\nBesuchernazahl: ").append(fest.getBesucherAnzahl()).append(
                                "\nPlz/Ort: ").append(fest.getPlz()).append(" ").append(fest.getOrt()).append(
                                "\nStrommanschluss: ").append(fest.getStromanschluss());

                        if (fest.getSpeisekarte() == 1) {
                            description.append("\n\nDie Speisekarten drucken wir");
                        } else {
                            description.append("\n\nDie Speisekarten wird nicht von uns gedruckt ");
                        }

                        if (fest.getKassa() == 1) {
                            description.append("\n\nDie Kassa wird von uns gemacht");
                        } else {
                            description.append("\n\nEs wird mit Bons kassiert");
                        }

                        description.append("\n\nAufstellen kann man ab: ").append(fest.getAufstellDatum().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

                        description.append("\n\n").append(fest.getAnmerkungen());

                        MailClient.sendMail(BASE_EMAIL, "Fest: " + fest.getFestName(), "Automatisch generierte Email\n\nMit freundlichen Grüßen\nIhr IT-Team",
                                addEvent(fest.getVeranstaltungsBeginn(), fest.getVeranstaltungsEnde(), fest.getOrt() + " " + fest.getAdresse(), description.toString(), fest.getFestName()));

                    }

                } catch (MessagingException | IOException e) {
                    LOGGER.warn("Event konnte nicht hinzugefügt werden", e);
                    throw new UserException(FacesMessage.SEVERITY_WARN, e);

                }
            }
        }).start();

    }

    private BodyPart addEvent(LocalDateTime begin, LocalDateTime ende, String location, String description, String summary) throws MessagingException, IOException {

        BodyPart calendarPart = new MimeBodyPart();

        DateFormat dataFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

        description = description.replaceAll("ö", "oe").replaceAll("ä", "ae").replaceAll("ü", "ue").replaceAll("\\n", "\\\\n");

        //check the icalendar spec in order to build a more complicated meeting request
        StringBuilder calendarContent = new StringBuilder();
        calendarContent.append("BEGIN:VCALENDAR\n").append("VERSION:2.0").append(
                "METHOD:PUBLISH\n").append("BEGIN:VEVENT\n").append("ORGANIZER:MAILTO:").append(BASE_EMAIL).append("\n").append(
                "DTSTART:").append(dataFormat.format(Date.from(begin.atZone(ZoneId.systemDefault()).toInstant()))).append("\n").append(
                "DTEND:").append(dataFormat.format(Date.from(ende.atZone(ZoneId.systemDefault()).toInstant()))).append("\n").append(
                "LOCATION:").append(location).append("\n").append(
                "UID:040000008200E00074C5B7101A82E00800000000002FF466CE3AC5010000000000000000100\n").append(" 000004377FE5C37984842BF9440448399EB02\n").append(
                "DESCRIPTION:").append(description).append("\n\n").append(
                "SUMMARY:Fest: ").append(summary).append("\n").append(
                "END:VEVENT\n").append("END:VCALENDAR");

        calendarPart.setHeader("Content-Class", "urn:content-  classes:calendarmessage");
        calendarPart.setHeader("Content-ID", "calendar_message");

        calendarPart.setDataHandler(new DataHandler(new ByteArrayDataSource(calendarContent.toString(), "text/calendar")));// very important

        return calendarPart;

    }

    public void setDefaultUser() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("user");
        } catch (Exception ex) {
        }
        User user = this.getUser(DEFAULT_USER);
        this.setCurrentUser(user);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
    }

    public void preLoad() {
        getRollen();
        getLangos();
        getHauptspeisen();
    }

    public void decreaseBestandByBestellung(Bestellung b) {
        int menge;
        int mindestbestand;
        StringBuilder sb = new StringBuilder();
        boolean sendMail = false;

        for (Bestellposition bp : getBestellpositionenByBestellID(b.getBestellID())) {
            menge = getLangos(bp.getLangosID()).getMenge();
            getLangos(bp.getLangosID()).setMenge(menge - bp.getMenge());

            mindestbestand = getLangos(bp.getLangosID()).getMindestbestand();
            menge = getLangos(bp.getLangosID()).getMenge();

            if (menge < mindestbestand) {
                sendMail = true;
                sb.append(getLangos(bp.getLangosID()).getName()).append("     ").append(mindestbestand).append("     ").append(menge).append("\n");
            }

        }

        if (sendMail) {
            MailClient.sendMail("Lagerbestandwarnung", getInformation(9).getText().replace("DETAILS", sb.toString()));
        }
    }

    public void decreaseBestandByFest(Fest f) {
        int menge;
        int mindestbestand;
        StringBuilder sb = new StringBuilder();
        boolean sendMail = false;

        for (FestLangos fl : getFestLangosByFestID(f.getID())) {
            menge = getLangos(fl.getLangosID()).getMenge();
            getLangos(fl.getLangosID()).setMenge(menge - fl.getMenge());

            mindestbestand = getLangos(fl.getLangosID()).getMindestbestand();
            menge = getLangos(fl.getLangosID()).getMenge();

            if (menge < mindestbestand) {
                sendMail = true;
                sb.append(getLangos(fl.getLangosID()).getName()).append("     ").append(mindestbestand).append("     ").append(menge).append("\n");
            }
        }
        if (sendMail) {
            MailClient.sendMail("Lagerbestandwarnung", getInformation(9).getText().replace("DETAILS", sb.toString()));
        }

    }

    public String getName(User user) {
        if (user.getTyp().equals(Typ.FIRMA) || user.getTyp().equals(Typ.VEREIN)) {
            return user.getFirmenname();
        }
        return user.getVorname() + " " + user.getNachname();
    }

}
