package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Rolle;

/**
 *
 * @author Kolar
 */
public interface RollenDAO extends BaseDAO<Rolle>{
    
}
