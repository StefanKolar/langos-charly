package at.htlpinkafeld.langoscharly.pojo;

/**
 *
 * @author DominikT
 */
public class Bestellposition implements Identifiable {
    private int bestellpositionID;
    private int bestellID;
    private int langosID;
    private int menge;

    public Bestellposition() {
    }

    public Bestellposition(int bestellpositionID, int bestellID, int langosID, int menge) {
        this(bestellID, langosID, menge);
        this.bestellpositionID = bestellpositionID;
    }
    
    public Bestellposition(int bestellID, int langosID, int menge) {
        this.bestellID = bestellID;
        this.langosID = langosID;
        this.menge = menge;
    }
    
    @Override
    public void setID(int id) {
        this.bestellpositionID = id;
    }

    @Override
    public int getID() {
        return this.bestellpositionID;
    }

    public int getBestellID() {
        return bestellID;
    }

    public void setBestellID(int bestellID) {
        this.bestellID = bestellID;
    }

    public int getLangosID() {
        return langosID;
    }

    public void setLangosID(int langosID) {
        this.langosID = langosID;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.bestellpositionID;
        hash = 29 * hash + this.bestellID;
        hash = 29 * hash + this.langosID;
        hash = 29 * hash + this.menge;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bestellposition other = (Bestellposition) obj;
        if (this.bestellpositionID != other.bestellpositionID) {
            return false;
        }
        if (this.bestellID != other.bestellID) {
            return false;
        }
        if (this.langosID != other.langosID) {
            return false;
        }
        if (this.menge != other.menge) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bestellposition{" + "bestellpositionID=" + bestellpositionID + ", bestellID=" + bestellID + ", langosID=" + langosID + ", menge=" + menge + '}';
    }
    
    
}
