package at.htlpinkafeld.langoscharly.pojo;

import java.io.Serializable;

/**
 *
 * @author Kolar
 */
public interface Identifiable extends Serializable{
    
    public void setID(int id);
    public int getID();

}
