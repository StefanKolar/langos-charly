package at.htlpinkafeld.langoscharly.pojo;

import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class Rolle implements Identifiable{
    
    private int rollenID;
    private String rolle;
    private boolean superadmin;
    private boolean admin;
    private boolean lagerbestand;
    private boolean kalender;
    private boolean bestellen;
    private boolean buchen;
    private boolean statisik;

    public Rolle(String rolle, boolean superadmin, boolean admin, boolean lagerbestand, boolean kalender, boolean bestellen, boolean buchen, boolean statisik) {
        this.rolle = rolle;
        this.superadmin = superadmin;
        this.admin = admin;
        this.lagerbestand = lagerbestand;
        this.kalender = kalender;
        this.bestellen = bestellen;
        this.buchen = buchen;
        this.statisik = statisik;
    }

    public Rolle(int rollenID, String rolle, boolean superadmin, boolean admin,  boolean lagerbestand, boolean kalender, boolean bestellen, boolean buchen, boolean statisik) {
        this(rolle, superadmin, admin, lagerbestand, kalender, bestellen, buchen, statisik);
        this.rollenID = rollenID;
    }

    @Override
    public int getID() {
        return rollenID;
    }

    @Override
    public void setID(int rollenID) {
        this.rollenID = rollenID;
    }
    
    
    public String getRolle() {
        return rolle;
    }

    public void setRolle(String rolle) {
        this.rolle = rolle;
    }

    public boolean isSuperadmin() {
        return superadmin;
    }

    public void setSuperadmin(boolean superadmin) {
        this.superadmin = superadmin;
    }

    public int getRollenID() {
        return rollenID;
    }

    public void setRollenID(int rollenID) {
        this.rollenID = rollenID;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isLagerbestand() {
        return lagerbestand;
    }

    public void setLagerbestand(boolean lagerbestand) {
        this.lagerbestand = lagerbestand;
    }

    public boolean isKalender() {
        return kalender;
    }

    public void setKalender(boolean kalender) {
        this.kalender = kalender;
    }

    public boolean isBestellen() {
        return bestellen;
    }

    public void setBestellen(boolean bestellen) {
        this.bestellen = bestellen;
    }

    public boolean isBuchen() {
        return buchen;
    }

    public void setBuchen(boolean buchen) {
        this.buchen = buchen;
    }

    public boolean isStatisik() {
        return statisik;
    }

    public void setStatisik(boolean statisik) {
        this.statisik = statisik;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.rollenID;
        hash = 41 * hash + Objects.hashCode(this.rolle);
        hash = 41 * hash + (this.superadmin ? 1 : 0);
        hash = 41 * hash + (this.admin ? 1 : 0);
        hash = 41 * hash + (this.lagerbestand ? 1 : 0);
        hash = 41 * hash + (this.kalender ? 1 : 0);
        hash = 41 * hash + (this.bestellen ? 1 : 0);
        hash = 41 * hash + (this.buchen ? 1 : 0);
        hash = 41 * hash + (this.statisik ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rolle other = (Rolle) obj;
        if (this.rollenID != other.rollenID) {
            return false;
        }
        if (this.superadmin != other.superadmin) {
            return false;
        }
        if (this.admin != other.admin) {
            return false;
        }
        if (this.lagerbestand != other.lagerbestand) {
            return false;
        }
        if (this.kalender != other.kalender) {
            return false;
        }
        if (this.bestellen != other.bestellen) {
            return false;
        }
        if (this.buchen != other.buchen) {
            return false;
        }
        if (this.statisik != other.statisik) {
            return false;
        }
        if (!Objects.equals(this.rolle, other.rolle)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rolle{" + "rollenID=" + rollenID + ", rolle=" + rolle + ", superadmin=" + superadmin + ", admin=" + admin + ", lagerbestand=" + lagerbestand + ", kalender=" + kalender + ", bestellen=" + bestellen + ", buchen=" + buchen + ", statisik=" + statisik + '}';
    }

    

    
    
}
