package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.interfaces.BestellDAO;
import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author DominikT
 */
public class BestellungJdbcDAO extends BaseJdbcDAO<Bestellung> implements BestellDAO {

    public BestellungJdbcDAO() {
        super("Bestellungen", "BestellID");
    }

    @Override
    protected Bestellung getPojoFromResultSet(ResultSet result) throws SQLException {

        int bestellID = result.getInt(super.getPkName());
        int userID = result.getInt("UserID");
        LocalDate datum = result.getObject("Datum", LocalDate.class);
        String ort = result.getString("Ort");
        int plz = result.getInt("PLZ");
        String adresse = result.getString("Adresse");
        boolean besteatigt = result.getBoolean("Bestätigt");

        return new Bestellung(bestellID, userID, datum, ort, plz, adresse, besteatigt);
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Bestellung b) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set UserID = ?, Datum = ?, Ort = ?, PLZ = ?, Adresse = ?, Bestätigt = ? where " + getPkName() + " = ?";

        PreparedStatement pStatement = c.prepareStatement(sql);

        pStatement.setInt(1, b.getUserID());
        pStatement.setObject(2, b.getDatum());
        pStatement.setString(3, b.getOrt());
        pStatement.setInt(4, b.getPlz());
        pStatement.setString(5, b.getAdresse());
        pStatement.setBoolean(6, b.isBestaetigt());
        pStatement.setInt(7, b.getBestellID());

        return pStatement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Bestellung b) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (UserID, Datum, Ort, PLZ, Adresse, Bestätigt) VALUES (?,?,?,?,?,?)";
        PreparedStatement pStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        pStatement.setInt(1, b.getUserID());
        pStatement.setObject(2, b.getDatum());
        pStatement.setString(3, b.getOrt());
        pStatement.setInt(4, b.getPlz());
        pStatement.setString(5, b.getAdresse());
        pStatement.setBoolean(6, b.isBestaetigt());

        return pStatement;
    }

    @Override
    public List<Bestellung> readBestellungByUser(User user) {
        List<Bestellung> bestellungList = new LinkedList<>();

        String sql = "SELECT * FROM " + super.getTablename() + " WHERE UserID = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, user.getID());
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        if (contains(result.getInt(getPkName()))) {
                            bestellungList.add(get(result.getInt(getPkName())));
                        } else {
                            bestellungList.add(getPojoFromResultSet(result));
                        }
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(BestellungJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return bestellungList;
    }

}
