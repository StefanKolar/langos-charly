package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Langos;
import java.io.InputStream;

/**
 *
 * @author Kolar
 */
public interface LangosDAO extends BaseDAO<Langos>{
    
    public InputStream readLangosImageByID(int id);
    public boolean updateImage(int id, InputStream bild);
    public int readMengeByID(int id);
    public boolean updateMenge(int id, int menge);

}
