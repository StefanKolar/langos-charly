package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.FestSpeisen;
import java.util.List;

/**
 *
 * @author kevinunger
 */
public interface FestSpeisenDAO extends BaseDAO<FestSpeisen>{
    
    public List<FestSpeisen> readFestSpeisenByFestID(int id);
    
}
