package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author CoolarPro
 */
public class LangosJdbcDAOTest implements TestDAOClass {

    private List<Langos> langos = new LinkedList<>();

    @Before
    public void init() {
        langos.add(new Langos("Kolar1", "Ein Test Langos", "A,C,B", 1000, 1500, false, ""));
        langos.add(new Langos("Kolar2", "Ein Test Langos", "A,C,B", 1000, 1500, false, ""));
        langos.add(new Langos("Kolar3", "Ein Test Langos", "A,C,B", 1000, 1500, false, ""));
        langos.add(new Langos("Kolar4", "Ein Test Langos", "A,C,B", 1000, 1500, false, ""));

        for (Langos l : langos) {
            DAOFactory.getInstance().createLangos(l);
        }
    }

    @After
    public void delete() {
        for (Langos l : langos) {
            DAOFactory.getInstance().deleteLangos(l);
        }
    }

    @Test
    @Override
    public void testUpdate() {
        assertEquals("Langos Without Update", langos.get(0), DAOFactory.getInstance().readLangos(langos.get(0).getID()));
        langos.get(0).setName("Update Langos Kolar");
        assertTrue("Update Langos", DAOFactory.getInstance().updateLangos(langos.get(0)));
        assertEquals("Update Langos Confirmation", langos.get(0), DAOFactory.getInstance().readLangos(langos.get(0).getID()));
    }

    @Test
    @Override
    public void testRead() {
        assertEquals("Read Langos 1", langos.get(0), DAOFactory.getInstance().readLangos(langos.get(0).getID()));
        assertEquals("Read Langos 2", langos.get(1), DAOFactory.getInstance().readLangos(langos.get(1).getID()));
        assertEquals("Read Langos 3", langos.get(2), DAOFactory.getInstance().readLangos(langos.get(2).getID()));
        assertEquals("Read Langos 4", langos.get(3), DAOFactory.getInstance().readLangos(langos.get(3).getID()));
    }

    @Test
    @Override
    public void testInsert() {
        Langos langos = new Langos("Kolar5", "Ein Test Langos", "A,C,B", 1000, 1500, false, "");
        this.langos.add(langos);
        assertTrue("Insert Langos", DAOFactory.getInstance().createLangos(langos) != null);
    }

    @Test
    @Override
    public void testDelete() {
        assertTrue("Delete Langos 1", DAOFactory.getInstance().deleteLangos(langos.get(0)));
        assertTrue("Delete Langos 2", DAOFactory.getInstance().deleteLangos(langos.get(1)));
        assertTrue("Delete Langos 3", DAOFactory.getInstance().deleteLangos(langos.get(2)));
        assertTrue("Delete Langos 4", DAOFactory.getInstance().deleteLangos(langos.get(3)));

        langos.clear();
    }
}
