package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Kolar
 */
public class HauptspeisenBean implements BaseBean {

    private LangosCharlyService service;

    private String name, beschreibung, allergene, bemerkung;
    private boolean verfuegbar;
    private InputStream bild;

    public HauptspeisenBean() {
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public List<Hauptspeise> getHauptspeisen() {
        return DAOFactory.getInstance().readAllHauptspeisen();
    }

    public String saveHauptspeisen(Hauptspeise hauptspeise) {
        service.createHauptspeise(hauptspeise);
        return null;
    }

    public String deleteHauptspeisen(Hauptspeise hauptspeise) {
        service.deleteHauptspeise(hauptspeise);

        return null;
    }

    public void updateHauptspeisen(Hauptspeise hauptspeise) {
        if (bild != null) {
            service.updateImage(hauptspeise, bild);
        }
        service.updateHauptspeise(hauptspeise);
    }

    public void cancel() {
        bild = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputStream getBild() {
        return bild;
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getAllergene() {
        return allergene;
    }

    public void setAllergene(String allergene) {
        this.allergene = allergene;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public boolean isVerfuegbar() {
        return verfuegbar;
    }

    public void setVerfuegbar(boolean verfuegbar) {
        this.verfuegbar = verfuegbar;
    }

    public void fileUpload(FileUploadEvent event) {
        try {
            bild = event.getFile().getInputstream();

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Bild hochgeladen", new FacesMessage("Erfolgreich", "Bild wurde erfolgreich hochgeladen"));
        } catch (IOException e) {
            Logger.getLogger(LangosBean.class).fatal("Bild nicht möglich hochzuladen: " + e.getMessage(), e.getCause());
        }

    }

    public String neueHauptspeise() {
        Hauptspeise hauptspeise = new Hauptspeise(name, beschreibung, allergene, verfuegbar, bemerkung);

        service.createHauptspeise(hauptspeise);

        name = null;
        beschreibung = null;
        allergene = null;
        bild = null;
        verfuegbar = false;
        bemerkung = null;

        return "hauptspeisen.xhtml?faces-redirect=true";
    }

    public List<Hauptspeise> getVerfuegbareHauptspeisen() {
        return service.getVerfuegbareHauptspeisen();
    }

}
