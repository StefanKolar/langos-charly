package at.htlpinkafeld.langoscharly.utilities;

import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class MailClient {

    private static final String E = "langos-charly@outlook.com";
    private static final String EMAILHOST = "smtp-mail.outlook.com";
    private static final String EMAILPASSWORD = "Julia1401";
    private static final String EMAILPORT = "587";

    private MailClient() {
        throw new AssertionError();
    }

    public static void sendMail(String email, String subject, String text) {

        String username = E;
        String password = EMAILPASSWORD;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", EMAILHOST);
        props.put("mail.smtp.port", EMAILPORT);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);

        } catch (MessagingException e) {
            Logger.getLogger(MailClient.class).fatal("Email konnte nicht versant werden");
            throw new UserException(FacesMessage.SEVERITY_WARN, e);
        }
    }

    public static void sendMail(String email, String subject, String text, BodyPart calendarPart) {

        String username = E;
        String password = EMAILPASSWORD;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", EMAILHOST);
        props.put("mail.smtp.port", EMAILPORT);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(subject);

            Multipart multipart = new MimeMultipart();

            //creates the Content of the Email
//            BodyPart t = new MimeBodyPart();
//            t.setContent(text, "text/html; charset=utf-8");
//            multipart.addBodyPart(t);
            //creates the contetn of the calender event
            multipart.addBodyPart(calendarPart);

            message.setContent(multipart);

            Transport.send(message);

        } catch (MessagingException e) {
            Logger.getLogger(MailClient.class).fatal("Email konnte nicht versant werden");
            throw new UserException(FacesMessage.SEVERITY_WARN, e);
        }
    }

    public static void sendMail(String subject, String text) {
        
        sendMail(LangosCharlyService.BASE_EMAIL, subject, text);
    }

}
