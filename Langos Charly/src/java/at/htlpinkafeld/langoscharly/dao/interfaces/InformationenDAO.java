package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Information;

/**
 *
 * @author Kolar
 */
public interface InformationenDAO extends BaseDAO<Information> {

}
