package at.htlpinkafeld.langoscharly.utilities;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Kolar
 */
public class PasswortEncryption {

    private PasswortEncryption() {
        throw new AssertionError();
    }

    // Der genutze Algorithmus, Nicht Verändern
    private static final String ALGORITHM = "PBKDF2WithHmacSHA512";

    // Wie lange es dauert um das Passwort zu überprüfen ( Min = 1000 | Max = 10 000 000 )
    private static final int ITERATION_COUNT = 1500;

    // Die länge des Keys
    private static final int KEY_LENGTH = 64;

    /**
     * Dise Methode returned ein encrypted Passwort im Typ BYTE[].
     *
     * @param password Das Passwort was encryptet wird
     * @param salt Zufällige Daten welche zum Hashen dienen
     * @return Ein encrypted Passwort als BYTE[]
     * @throws NoSuchAlgorithmException Wenn der cryptohraphic algorithmus
     * fehlschlägt
     * @throws InvalidKeySpecException Falls der Key nicht erzeugt werden kann
     */
    public static byte[] hash(final String password, final byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        final KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        final SecretKeyFactory secretKeyfactory = SecretKeyFactory.getInstance(ALGORITHM);
        return secretKeyfactory.generateSecret(keySpec).getEncoded();
    }

    /**
     * Generiert ein zufälliges Salt
     *
     * @return Ein zufälliger Byte[]
     *
     * @throws NoSuchAlgorithmException If SHA1PRNG does not exist on the
     * system.
     */
    public static byte[] salt() throws NoSuchAlgorithmException {
        final byte[] salt = new byte[16];
        SecureRandom.getInstance("SHA1PRNG").nextBytes(salt);
        return salt;
    }

    /**
     * Convert ein String zu einen BYTE[] Zum Beispiel byte[] test = {1, 2, 3,
     * 4} this method will convert that into a String that looks like this: "1 2
     * 3 4";
     *
     * Erlaub uns ein Array in die Datenbank zu speichern
     *
     * @param payload Die BYTE[] welche in ein String konvertiert werden soll
     * @return Der Konvertierte String
     */
    public static String convertToString(final byte[] payload) {
        String result = "";
        for (byte b : payload) {
            result += b + " ";
        }
        return result.trim();
    }

    /**
     * Convertiert von String zu BYTE[]
     *
     * @param s Der String der Convertiert werden muss
     * @return Der Convertierte ByTE[]
     */
    public static byte[] toByteArray(final String s) {
        final String[] arr = s.split(" ");
        final byte[] b = new byte[arr.length];
        for (int i = 0; i < arr.length; i++) {
            b[i] = Byte.parseByte(arr[i]);
        }
        return b;
    }

    /**
     * Überprüft das Passwort
     *
     * @param attemptedPassword Das Passwort was überprüft werden soll
     * @param hashedPassword Das gehashde Passwort aus der Datenbank
     * @param salt Das Salt was benutz wurde
     * @return Ob das Passwort übereinstimmt
     * @throws Exception Wenn ein Fehler auftritt
     */
    public static boolean authenticate(final String attemptedPassword, final byte[] salt, final byte[] hashedPassword) throws Exception {
        return Arrays.equals(hash(attemptedPassword, salt), hashedPassword);
    }

}
