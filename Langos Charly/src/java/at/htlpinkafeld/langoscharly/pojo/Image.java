package at.htlpinkafeld.langoscharly.pojo;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import java.io.InputStream;
import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class Image implements Identifiable {

    private int bildID;
    private String name;
    private InputStream bild;

    public Image(int bildID, String name, InputStream bild) {
        this(name, bild);
        this.bildID = bildID;
    }

    public Image(String name, InputStream bild) {
        this.name = name;
        this.bild = bild;
    }

    @Override
    public void setID(int id) {
        this.bildID = id;
    }

    @Override
    public int getID() {
        return this.bildID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputStream getBild() {
        return DAOFactory.getInstance().readImageBildByID(bildID);
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.bildID;
        hash = 19 * hash + Objects.hashCode(this.name);
        hash = 19 * hash + Objects.hashCode(this.bild);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Image other = (Image) obj;
        if (this.bildID != other.bildID) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.bild, other.bild)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Image{" + "bildID=" + bildID + ", name=" + name + ", bild=" + bild + '}';
    }
}
