package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.interfaces.FestLangosDAO;
import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author kevinunger
 */
public class FestLangosJdbcDAO extends BaseJdbcDAO<FestLangos> implements FestLangosDAO {

    public FestLangosJdbcDAO() {
        super("FestLangos", null);
    }

    @Override
    protected FestLangos getPojoFromResultSet(ResultSet result) throws SQLException {
        int festID = result.getInt("FestID");
        int langosID = result.getInt("LangosID");
        int menge = result.getInt("Menge");

        return new FestLangos(festID, langosID, menge);
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, FestLangos t) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Menge = ?, where FestID = ? and LangosID = ?";

        PreparedStatement pStatement = c.prepareStatement(sql);

        pStatement.setInt(1, t.getMenge());
        pStatement.setInt(2, t.getID());
        pStatement.setInt(3, t.getLangosID());

        return pStatement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, FestLangos t) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (FestID, LangosID, Menge) VALUES (?,?,?)";
        PreparedStatement pStatement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        pStatement.setInt(1, t.getID());
        pStatement.setInt(2, t.getLangosID());
        pStatement.setInt(3, t.getMenge());

        return pStatement;
    }

    @Override
    public List<FestLangos> readFestLangosByFestID(int id) {
        List<FestLangos> festLangosList = new LinkedList<>();

        String sql = "SELECT * FROM " + super.getTablename() + " WHERE FestID = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        festLangosList.add(getPojoFromResultSet(result));
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(BestellungJdbcDAO.class).log(Level.FATAL, "Not able to create a connection", ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return festLangosList;
    }

}
