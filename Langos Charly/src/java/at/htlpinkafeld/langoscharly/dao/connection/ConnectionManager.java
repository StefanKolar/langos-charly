package at.htlpinkafeld.langoscharly.dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Kolar
 */
public class ConnectionManager {

    private static final String DATASOURCE = "mysql/mysql-8.0.13";
    private static ConnectionManager connMgrInst = null;
    private DataSource ds = null;
    private final static String URL = "jdbc:mysql://85.126.154.26:3306/langosci_langos_charly?serverTimezone=CET";
    private final static String USERNAME = "lango_admin";
    private final static String PW = "Chg7423!";

    public static synchronized ConnectionManager getInstance() {
        if (connMgrInst == null) {
            connMgrInst = new ConnectionManager();
        }

        return connMgrInst;
    }

    private ConnectionManager() {
        try {
            Context ctx;
            ctx = new javax.naming.InitialContext();
            ds = (DataSource) ctx.lookup("java:comp/env/" + DATASOURCE);
        } catch (NamingException ex) {
            Logger.getLogger(ConnectionManager.class).error(ex.getMessage());
        }
    }

    public Connection getConnection() {
        Connection retVal = null;
        try {
            retVal = ds.getConnection();
        } catch (Exception ex) {
            Logger.getLogger(ConnectionManager.class).error(ex.getMessage());
        }
        if (retVal == null) {
            try {
                retVal = DriverManager.getConnection(URL, USERNAME, PW);
            } catch (SQLException ex) {
                Logger.getLogger(ConnectionManager.class).error(ex.getMessage());
            }

        }

        return retVal;
    }
}
