package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import at.htlpinkafeld.langoscharly.dao.interfaces.HauptspeisenDAO;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.InputStream;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class HauptspeisenJdbcDAO extends BaseJdbcDAO<Hauptspeise> implements HauptspeisenDAO {

    public HauptspeisenJdbcDAO() {
        super("Hauptspeisen", "HauptspeisenID");
    }

    @Override
    protected String readSQLStatment() {
        return "SELECT " + getPkName() + ", Name, Beschreibung, Allergene, Verfügbar, Bemerkung FROM " + super.getTablename();
    }

    @Override
    protected Hauptspeise getPojoFromResultSet(ResultSet result) throws SQLException {

        int hauptspeisenID = result.getInt(getPkName());
        String name = result.getString("Name");
        String beschreibung = result.getString("Beschreibung");
        String allergene = result.getString("Allergene");
        boolean verfuegbar = result.getBoolean("Verfügbar");
        String bemerkung = result.getString("Bemerkung");

        return new Hauptspeise(hauptspeisenID, name, beschreibung, allergene, verfuegbar, bemerkung);
    }

    @Override
    public InputStream readHauptspeisenImageByID(int id) {

        String sql = "SELECT Bild FROM " + super.getTablename() + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        return result.getBinaryStream("Bild");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return null;
    }

    @Override
    public boolean updateImage(int id, InputStream bild) {

        String sql = "UPDATE " + super.getTablename() + " set Bild = ?" + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setBinaryStream(1, bild);
                statement.setInt(2, id);
                if (statement.executeUpdate() > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Hauptspeise hs) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Name = ?, Beschreibung = ?, Allergene = ?, Verfügbar = ?, Bemerkung = ? where " + getPkName() + " = ?";

        PreparedStatement statement = c.prepareStatement(sql);
        statement.setString(1, hs.getName());
        statement.setString(2, hs.getBeschreibung());
        statement.setString(3, hs.getAllergene());
        statement.setBoolean(4, hs.isVerfuegbar());
        statement.setString(5, hs.getBemerkung());
        statement.setInt(6, hs.getID());

        return statement;

    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Hauptspeise hs) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Name, Beschreibung, Allergene, Verfügbar, Bemerkung) VALUES (?,?,?,?,?)";
        PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, hs.getName());
        statement.setString(2, hs.getBeschreibung());
        statement.setString(3, hs.getAllergene());
        statement.setBoolean(4, hs.isVerfuegbar());
        statement.setString(5, hs.getBemerkung());

        return statement;
    }

}
