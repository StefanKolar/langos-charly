package at.htlpinkafeld.langoscharly.gui.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author DominikT
 */
public class PLZValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        if(o != null) {
            int plz = (int) o;
            
            if(plz <= 0) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bitte eine gültige PLZ für Österreich angeben.", null));
            }
        }
    }
    
}
