package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author CoolarPro
 */
public class RollenJdbcDAOTest implements TestDAOClass {

    private List<Rolle> rollen = new LinkedList<>();

    @Before
    public void init() {
        rollen.add(new Rolle("Test1", true, true, true, true, true, true, true));
        rollen.add(new Rolle("Test2", false, true, true, false, true, true, false));
        rollen.add(new Rolle("Test3", false, true, true, false, true, false, true));
        rollen.add(new Rolle("Test4", false, true, false, true, false, true, true));

        for (Rolle rolle : rollen) {
            DAOFactory.getInstance().createRolle(rolle);
        }
    }

    @After
    public void delete() {
        for (Rolle rolle : rollen) {
            DAOFactory.getInstance().deleteRolle(rolle);
        }
    }

    @Test
    @Override
    public void testUpdate() {
        assertEquals("Rolle Without Update", rollen.get(0), DAOFactory.getInstance().readRolle(rollen.get(0).getID()));
        rollen.get(0).setSuperadmin(false);
        assertTrue("Update Rolle by Rolle", DAOFactory.getInstance().updateRolle(rollen.get(0)));
        assertEquals("Update Rolle Confirmation", rollen.get(0), DAOFactory.getInstance().readRolle(rollen.get(0).getID()));
    }

    @Test
    @Override
    public void testRead() {
        assertEquals("Read Rolle 1", rollen.get(0), DAOFactory.getInstance().readRolle(rollen.get(0).getID()));
        assertEquals("Read Rolle 2", rollen.get(1), DAOFactory.getInstance().readRolle(rollen.get(1).getID()));
        assertEquals("Read Rolle 3", rollen.get(2), DAOFactory.getInstance().readRolle(rollen.get(2).getID()));
        assertEquals("Read Rolle 4", rollen.get(3), DAOFactory.getInstance().readRolle(rollen.get(3).getID()));
    }

    @Test
    @Override
    public void testInsert() {
        Rolle rolle = new Rolle("Test4", true, true, true, true, true, true, true);
        rollen.add(rolle);
        assertTrue("Insert Rolle", DAOFactory.getInstance().createRolle(rolle) != null);
    }

    @Test
    @Override
    public void testDelete() {
        assertTrue("Delete Rolle 1", DAOFactory.getInstance().deleteRolle(rollen.get(0)));
        assertTrue("Delete Rolle 2", DAOFactory.getInstance().deleteRolle(rollen.get(1)));
        assertTrue("Delete Rolle 3", DAOFactory.getInstance().deleteRolle(rollen.get(2)));
        assertTrue("Delete Rolle 4", DAOFactory.getInstance().deleteRolle(rollen.get(3)));

        rollen.clear();
    }
}
