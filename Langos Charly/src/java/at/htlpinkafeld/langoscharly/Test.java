package at.htlpinkafeld.langoscharly;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import java.sql.SQLException;

/**
 *
 * @author Kolar
 */
public class Test {
    
    public static void main(String[] args) throws SQLException, InterruptedException{
//        MailClient.sendMail("kolar.stefan@gmx.net", "Passwort vergessen", "test");

        DAOFactory.getInstance().createRolle(new Rolle("Test", true, true, true, true, true, true, true));
    }
    
}
