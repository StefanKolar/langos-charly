package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import javax.annotation.PostConstruct;

/**
 *
 * @author Kolar
 */
public class MenuBean implements BaseBean {

    private User user;
    private LangosCharlyService service;

    public MenuBean() {
    }

    @PostConstruct
    public void init() {
        user = service.getCurrentUser();
    }

    public boolean isGast() {
        return user.getFirmenname().equals(LangosCharlyService.GAST);
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public boolean isBestellen() {

        if (user != null) {
            if (user.getRolle().isBestellen()) {
                return true;
            }
        }

        return false;

    }

    public boolean isKalender() {
        if (user != null) {
            if (user.getRolle().isKalender()) {
                return true;
            }
        }

        return false;
    }

    public boolean isStatistik() {
        if (user != null) {
            if (user.getRolle().isStatisik()) {
                return true;
            }
        }

        return false;
    }

    public boolean isLagerbestand() {
        if (user != null) {
            if (user.getRolle().isLagerbestand()) {
                return true;
            }
        }

        return false;
    }

    public boolean isAdmin() {
        if (user != null) {
            if (user.getRolle().isAdmin()) {
                return true;
            }
        }

        return false;
    }

    public boolean isBuchen() {
        if (user != null) {
            if (user.getRolle().isBuchen()) {
                return true;
            }
        }

        return false;
    }

    public boolean isSuperAdmin() {
        if (user != null) {
            if (user.getRolle().isSuperadmin()) {
                return true;
            }
        }

        return false;
    }

    public boolean isShowAdminArea() {
        if (user != null) {
            if (user.getRolle().isSuperadmin()) {
                return true;
            }
            if (user.getRolle().isStatisik()){
                return true;
            }
            if(user.getRolle().isAdmin()) {
                return true;
            }
        }

        return false;
    }
    
}
