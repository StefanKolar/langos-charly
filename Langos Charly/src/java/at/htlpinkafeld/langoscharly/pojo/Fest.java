package at.htlpinkafeld.langoscharly.pojo;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author kevinunger
 */
public class Fest implements Identifiable {

    private int festBuchenID;
    private int userID;
    private String festName;
    private String vorname;
    private String nachname;
    private String telefonnummer;
    private String email;
    private int besucherAnzahl;
    private int speisekarte; //1 für LC
    private int kassa; //1 für LC
    private LocalDateTime veranstaltungsBeginn; //datetime in DB
    private LocalDateTime veranstaltungsEnde; //datetime in DB
    private LocalDateTime aufstellDatum;
    private String anmerkungen;
    private String ort;
    private int plz;
    private String adresse;
    private String stromanschluss;
    private boolean bestaetigt;
    private boolean abgeschlossen;

    public Fest() {
    }

    public Fest(int festBuchenID, int userID, String festName, String vorname, String nachname, String email, String telefonnummer, String ort, int plz, String adresse, LocalDateTime aufstellDatum, LocalDateTime veranstaltungsBeginn, LocalDateTime veranstaltungsEnde, String stromanschluss, int besucherAnzahl, int speisekarte, int kassa, String anmerkungen, boolean besteatigt, boolean abgeschlossen) {
        this.festBuchenID = festBuchenID;
        this.userID = userID;
        this.festName = festName;
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.telefonnummer = telefonnummer;
        this.ort = ort;
        this.plz = plz;
        this.adresse = adresse;
        this.aufstellDatum = aufstellDatum;
        this.veranstaltungsBeginn = veranstaltungsBeginn;
        this.veranstaltungsEnde = veranstaltungsEnde;
        this.stromanschluss = stromanschluss;
        this.besucherAnzahl = besucherAnzahl;
        this.speisekarte = speisekarte;
        this.kassa = kassa;
        this.anmerkungen = anmerkungen;
        this.bestaetigt = besteatigt;
        this.abgeschlossen = abgeschlossen;
    }

    public Fest(int userID, String festName, String vorname, String nachname, String telefonnummer, String email, int besucherAnzahl, int speisekarte, int kassa, LocalDateTime veranstaltungsBeginn, LocalDateTime veranstaltungsEnde, LocalDateTime aufstellDatum, String anmerkungen, String ort, int plz, String adresse, String stromanschluss) {
        this.userID = userID;
        this.festName = festName;
        this.vorname = vorname;
        this.nachname = nachname;
        this.telefonnummer = telefonnummer;
        this.email = email;
        this.besucherAnzahl = besucherAnzahl;
        this.speisekarte = speisekarte;
        this.kassa = kassa;
        this.veranstaltungsBeginn = veranstaltungsBeginn;
        this.veranstaltungsEnde = veranstaltungsEnde;
        this.aufstellDatum = aufstellDatum;
        this.anmerkungen = anmerkungen;
        this.ort = ort;
        this.plz = plz;
        this.adresse = adresse;
        this.stromanschluss = stromanschluss;
    }

    public Fest(int festBuchenID, int userID, String festName, String vorname, String nachname, String telefonnummer, String email, int besucherAnzahl, int speisekarte, int kassa, LocalDateTime veranstaltungsBeginn, LocalDateTime veranstaltungsEnde, LocalDateTime aufstellDatum, String anmerkungen, String ort, int plz, String adresse, String stromanschluss) {
        this.festBuchenID = festBuchenID;
        this.userID = userID;
        this.festName = festName;
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.telefonnummer = telefonnummer;
        this.ort = ort;
        this.plz = plz;
        this.adresse = adresse;
        this.aufstellDatum = aufstellDatum;
        this.veranstaltungsBeginn = veranstaltungsBeginn;
        this.veranstaltungsEnde = veranstaltungsEnde;
        this.stromanschluss = stromanschluss;
        this.besucherAnzahl = besucherAnzahl;
        this.speisekarte = speisekarte;
        this.kassa = kassa;
        this.anmerkungen = anmerkungen;
    }

    @Override
    public void setID(int id) {
        this.festBuchenID = id;
    }

    @Override
    public int getID() {
        return festBuchenID;
    }

    public int getFestBuchenID() {
        return festBuchenID;
    }

    public void setFestBuchenID(int festBuchenID) {
        this.festBuchenID = festBuchenID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFestName() {
        return festName;
    }

    public void setFestName(String festName) {
        this.festName = festName;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBesucherAnzahl() {
        return besucherAnzahl;
    }

    public void setBesucherAnzahl(int besucherAnzahl) {
        this.besucherAnzahl = besucherAnzahl;
    }

    public int getSpeisekarte() {
        return speisekarte;
    }

    public void setSpeisekarte(int speisekarte) {
        this.speisekarte = speisekarte;
    }

    public int getKassa() {
        return kassa;
    }

    public void setKassa(int kassa) {
        this.kassa = kassa;
    }

    public LocalDateTime getVeranstaltungsBeginn() {
        return veranstaltungsBeginn;
    }

    public void setVeranstaltungsBeginn(LocalDateTime veranstaltungsBeginn) {
        this.veranstaltungsBeginn = veranstaltungsBeginn;
    }

    public LocalDateTime getVeranstaltungsEnde() {
        return veranstaltungsEnde;
    }

    public void setVeranstaltungsEnde(LocalDateTime veranstaltungsEnde) {
        this.veranstaltungsEnde = veranstaltungsEnde;
    }

    public LocalDateTime getAufstellDatum() {
        return aufstellDatum;
    }

    public void setAufstellDatum(LocalDateTime aufstellDatum) {
        this.aufstellDatum = aufstellDatum;
    }

    public String getAnmerkungen() {
        return anmerkungen;
    }

    public void setAnmerkungen(String anmerkungen) {
        this.anmerkungen = anmerkungen;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getStromanschluss() {
        return stromanschluss;
    }

    public void setStromanschluss(String stromanschluss) {
        this.stromanschluss = stromanschluss;
    }

    public boolean isBestaetigt() {
        return bestaetigt;
    }

    public void setBestaetigt(boolean bestaetigt) {
        this.bestaetigt = bestaetigt;
    }

    public boolean isAbgeschlossen() {
        return abgeschlossen;
    }

    public void setAbgeschlossen(boolean abgeschlossen) {
        this.abgeschlossen = abgeschlossen;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.festBuchenID;
        hash = 89 * hash + this.userID;
        hash = 89 * hash + Objects.hashCode(this.festName);
        hash = 89 * hash + Objects.hashCode(this.vorname);
        hash = 89 * hash + Objects.hashCode(this.nachname);
        hash = 89 * hash + Objects.hashCode(this.telefonnummer);
        hash = 89 * hash + Objects.hashCode(this.email);
        hash = 89 * hash + this.besucherAnzahl;
        hash = 89 * hash + this.speisekarte;
        hash = 89 * hash + this.kassa;
        hash = 89 * hash + Objects.hashCode(this.veranstaltungsBeginn);
        hash = 89 * hash + Objects.hashCode(this.veranstaltungsEnde);
        hash = 89 * hash + Objects.hashCode(this.aufstellDatum);
        hash = 89 * hash + Objects.hashCode(this.anmerkungen);
        hash = 89 * hash + Objects.hashCode(this.ort);
        hash = 89 * hash + this.plz;
        hash = 89 * hash + Objects.hashCode(this.adresse);
        hash = 89 * hash + Objects.hashCode(this.stromanschluss);
        hash = 89 * hash + (this.bestaetigt ? 1 : 0);
        hash = 89 * hash + (this.abgeschlossen ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fest other = (Fest) obj;
        if (this.festBuchenID != other.festBuchenID) {
            return false;
        }
        if (this.userID != other.userID) {
            return false;
        }
        if (this.besucherAnzahl != other.besucherAnzahl) {
            return false;
        }
        if (this.speisekarte != other.speisekarte) {
            return false;
        }
        if (this.kassa != other.kassa) {
            return false;
        }
        if (this.plz != other.plz) {
            return false;
        }
        if (this.bestaetigt != other.bestaetigt) {
            return false;
        }
        if (this.abgeschlossen != other.abgeschlossen) {
            return false;
        }
        if (!Objects.equals(this.festName, other.festName)) {
            return false;
        }
        if (!Objects.equals(this.vorname, other.vorname)) {
            return false;
        }
        if (!Objects.equals(this.nachname, other.nachname)) {
            return false;
        }
        if (!Objects.equals(this.telefonnummer, other.telefonnummer)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.anmerkungen, other.anmerkungen)) {
            return false;
        }
        if (!Objects.equals(this.ort, other.ort)) {
            return false;
        }
        if (!Objects.equals(this.adresse, other.adresse)) {
            return false;
        }
        if (!Objects.equals(this.stromanschluss, other.stromanschluss)) {
            return false;
        }
        if (!Objects.equals(this.veranstaltungsBeginn, other.veranstaltungsBeginn)) {
            return false;
        }
        if (!Objects.equals(this.veranstaltungsEnde, other.veranstaltungsEnde)) {
            return false;
        }
        if (!Objects.equals(this.aufstellDatum, other.aufstellDatum)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FestBuchen{" + "festBuchenID=" + festBuchenID + ", userID=" + userID + ", festName=" + festName + ", vorname=" + vorname + ", nachname=" + nachname + ", telefonnummer=" + telefonnummer + ", email=" + email + ", besucherAnzahl=" + besucherAnzahl + ", speisekarte=" + speisekarte + ", kassa=" + kassa + ", veranstaltungsBeginn=" + veranstaltungsBeginn + ", veranstaltungsEnde=" + veranstaltungsEnde + ", aufstellDatum=" + aufstellDatum + ", anmerkungen=" + anmerkungen + ", ort=" + ort + ", plz=" + plz + ", adresse=" + adresse + ", stromanschluss=" + stromanschluss + ", bestaetigt=" + bestaetigt + ", abgeschlossen=" + abgeschlossen + '}';
    }

}
