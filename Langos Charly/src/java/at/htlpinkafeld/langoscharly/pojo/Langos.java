package at.htlpinkafeld.langoscharly.pojo;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import java.io.InputStream;
import java.util.Objects;

/**
 *
 * @author Kolar
 */
public class Langos implements Identifiable {

    private int langosID;
    private String name;
    private String beschreibung;
    private String allergene;
    private InputStream bild;
    private int mindestbestand;
    private int menge;
    private boolean verfuegbar;
    private String bemerkung;

    public Langos(String name, String beschreibung, String allergene, int mindestbestand, boolean verfuegbar, String bemerkung) {
        this.name = name;
        this.beschreibung = beschreibung;
        this.allergene = allergene;
        this.mindestbestand = mindestbestand;
        this.verfuegbar = verfuegbar;
        this.bemerkung = bemerkung;
    }

    public Langos(int langosID, String name, String beschreibung, String allergene, int mindestbestand, boolean verfuegbar, String bemerkung) {
        this(name, beschreibung, allergene, mindestbestand, verfuegbar, bemerkung);
        this.langosID = langosID;
    }

    public Langos(String name, String beschreibung, String allergene, int mindestbestand, int menge, boolean verfuegbar, String bemerkung) {
        this(name, beschreibung, allergene, mindestbestand, verfuegbar, bemerkung);
        this.menge = menge;
    }

    public Langos(int langosID, String name, String beschreibung, String allergene, int mindestbestand, int menge, boolean verfuegbar, String bemerkung) {
        this(name, beschreibung, allergene, mindestbestand, menge, verfuegbar, bemerkung);
        this.langosID = langosID;
    }

    @Override
    public int getID() {
        return langosID;
    }

    @Override
    public void setID(int langosID) {
        this.langosID = langosID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getAllergene() {
        return allergene;
    }

    public void setAllergene(String allergene) {
        this.allergene = allergene;
    }

    public InputStream getBild() {
        return DAOFactory.getInstance().readLangosImageByID(langosID);
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    public int getMindestbestand() {
        return mindestbestand;
    }

    public void setMindestbestand(int mindestbestand) {
        this.mindestbestand = mindestbestand;
    }

    public int getMenge() {
        return DAOFactory.getInstance().readLangosMengeByID(langosID);
    }

    public void setMenge(int menge) {
        DAOFactory.getInstance().updateMenge(langosID, menge);
    }

    
    
    public boolean isVerfuegbar() {
        return verfuegbar;
    }

    public void setVerfuegbar(boolean verfuegbar) {
        this.verfuegbar = verfuegbar;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.langosID;
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.beschreibung);
        hash = 41 * hash + Objects.hashCode(this.allergene);
        hash = 41 * hash + Objects.hashCode(this.bild);
        hash = 41 * hash + this.mindestbestand;
        hash = 41 * hash + this.menge;
        hash = 41 * hash + (this.verfuegbar ? 1 : 0);
        hash = 41 * hash + Objects.hashCode(this.bemerkung);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Langos other = (Langos) obj;
        if (this.langosID != other.langosID) {
            return false;
        }
        if (this.mindestbestand != other.mindestbestand) {
            return false;
        }
        if (this.menge != other.menge) {
            return false;
        }
        if (this.verfuegbar != other.verfuegbar) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.beschreibung, other.beschreibung)) {
            return false;
        }
        if (!Objects.equals(this.allergene, other.allergene)) {
            return false;
        }
        if (!Objects.equals(this.bild, other.bild)) {
            return false;
        }
        if (!Objects.equals(this.bemerkung, other.bemerkung)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Langos{" + "langosID=" + langosID + ", name=" + name + ", beschreibung=" + beschreibung + ", allergene=" + allergene + ", bild=" + bild + ", mindestbestand=" + mindestbestand + ", menge=" + menge + ", verfuegbar=" + verfuegbar + ", bemerkung=" + bemerkung + '}';
    }
    
    
}
