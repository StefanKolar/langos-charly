
package at.htlpinkafeld.langoscharly.dao.jdcb;

import org.junit.Test;

/**
 *
 * @author Kolar
 */
public interface TestDAOClass {
    
    @Test
    public void testInsert();
    
    @Test
    public void testUpdate();
    
    @Test
    public void testDelete();
    
    @Test
    public void testRead();

}
