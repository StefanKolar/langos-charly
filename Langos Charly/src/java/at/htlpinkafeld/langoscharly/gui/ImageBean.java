package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Kolar
 */
public class ImageBean implements BaseBean {

    private static final int IMAGESCALE = 300;
    private static final int IMAGESCALEFORMAINHEIGHT = 450; //450
    private static final int IMAGESCALEFORMAINWIDTH = 750; //750
    
    private InputStream b;
    private List<at.htlpinkafeld.langoscharly.pojo.Image> bilder = new LinkedList<>();

    public ImageBean() {
    }

    private LangosCharlyService service;

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public List<at.htlpinkafeld.langoscharly.pojo.Image> getBilder() {
        if (bilder.isEmpty()) {
            bilder = this.service.getBilder();
        }
        return bilder.stream().sorted(( o1, o2) -> ((Integer) o1.getID()).compareTo(o2.getID())).collect(Collectors.toList());
    }

    public void fileUpload(FileUploadEvent event) {
        try {
            b = event.getFile().getInputstream();
            
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Bild hochgeladen", new FacesMessage("Erfolgreich", "Bild wurde erfolgreich hochgeladen"));
        } catch (IOException e) {
            Logger.getLogger(LangosBean.class).fatal("Bild nicht möglich hochzuladen: " + e.getMessage(), e);
        }

    }

    public void upload(at.htlpinkafeld.langoscharly.pojo.Image bild){
        this.service.updateImage(bild, b);
    }
    
    private StreamedContent getImagefromLangos(int id) throws IOException {

        Langos langos = service.getLangos(id);

        DefaultStreamedContent retVal;

        if (langos.getBild() != null) {

            InputStream bild = scaleImage(getByteArrayInputStream(langos.getBild()), IMAGESCALE, IMAGESCALE);

            retVal = new DefaultStreamedContent(bild, "image/jpeg");

        } else {
            retVal = new DefaultStreamedContent();
        }

        return retVal;
    }

    private StreamedContent getImagefromHauptspeise(int id) throws IOException {

        Hauptspeise hauptspeise = service.getHauptspeise(id);

        DefaultStreamedContent retVal;

        if (hauptspeise.getBild() != null && hauptspeise.getBild().markSupported()) {

            hauptspeise.getBild().mark(Integer.MAX_VALUE);

            InputStream bild = scaleImage(getByteArrayInputStream(hauptspeise.getBild()), IMAGESCALE, IMAGESCALE);

            hauptspeise.getBild().reset();

            retVal = new DefaultStreamedContent(bild, "image/jpeg");

        } else {
            retVal = new DefaultStreamedContent();
        }

        return retVal;
    }

    private StreamedContent getImagefromBild(Integer id) throws IOException {

        at.htlpinkafeld.langoscharly.pojo.Image image = service.getBild(id);

        DefaultStreamedContent retVal;

        if (image.getBild() != null) {

            InputStream bild = scaleImage(getByteArrayInputStream(image.getBild()), IMAGESCALEFORMAINHEIGHT, IMAGESCALEFORMAINWIDTH);
            
            retVal = new DefaultStreamedContent(bild, "image/jpeg");

        } else {
            retVal = new DefaultStreamedContent();
        }

        return retVal;

    }

    /**
     * Returns a ByteArray of the InputStream
     */
    private ByteArrayInputStream getByteArrayInputStream(InputStream bild) throws IOException {
        byte[] buff = new byte[8000];

        int bytesRead;

        ByteArrayOutputStream bao = new ByteArrayOutputStream();

        while ((bytesRead = bild.read(buff)) != -1) {
            bao.write(buff, 0, bytesRead);
        }

        return new ByteArrayInputStream(bao.toByteArray());
    }

    /**
     * Scale the InputStream to the predefined IMAGESCALE
     */
    private ByteArrayInputStream scaleImage(InputStream bild, int SCALEHEIGHT, int SCALEWIDTH) throws IOException {
        BufferedImage sourceImage = ImageIO.read(bild);
        Image thumbnail = sourceImage.getScaledInstance(SCALEWIDTH, SCALEHEIGHT, Image.SCALE_SMOOTH);
        BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
                thumbnail.getHeight(null),
                BufferedImage.TYPE_INT_RGB);
        bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedThumbnail, "jpeg", baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    public StreamedContent getImage() throws IOException {

        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {

        }
        String id = context.getExternalContext().getRequestParameterMap().get("langosID");

        if (id != null) {
            return getImagefromLangos(Integer.valueOf(id));
        } else {
            id = context.getExternalContext().getRequestParameterMap().get("hauptspeisenID");
            if (id != null) {
                return getImagefromHauptspeise(Integer.valueOf(id));
            }
            id = context.getExternalContext().getRequestParameterMap().get("bild");
            if (id != null) {
                return getImagefromBild(Integer.valueOf(id));
            }
        }

        return new DefaultStreamedContent();
    }
    
    public boolean showImage(int id){
        return service.getBild(id).getBild() != null;
    }

}
