package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Kolar
 */
public class RollenBean implements BaseBean {

    private LangosCharlyService service;

    private String name;
    private boolean superadmin, admin, lagerbestand, statistik, bestellen, buchen, kalender;

    public RollenBean() {
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public List<Rolle> getRollen() {
        return DAOFactory.getInstance().readAllRollen();
    }

    public void saveRolle(Rolle rolle) {
        service.updateRolle(rolle);
    }

    public String deleteRolle(Rolle rolle) {
        try {
            service.deleteRolle(rolle);
        } catch (UserException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(ex.getSeverity(), "Löschen nicht möglich", "Ein oder mehrere User sind noch dieser Rolle zugeordnet. Ändern Sie diese um die Rolle löschen zu können."));
        }

        return null;
    }

    public Rolle getRolleByName(String name) {
        return service.getRolleByName(name);
    }

    public boolean render(Rolle rolle) {
        return rolle.getID() > 6;
    }

    public LangosCharlyService getService() {
        return service;
    }

    public void setService(LangosCharlyService service) {
        this.service = service;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSuperadmin() {
        return superadmin;
    }

    public void setSuperadmin(boolean superadmin) {
        this.superadmin = superadmin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isLagerbestand() {
        return lagerbestand;
    }

    public void setLagerbestand(boolean lagerbestand) {
        this.lagerbestand = lagerbestand;
    }

    public boolean isStatistik() {
        return statistik;
    }

    public void setStatistik(boolean statistik) {
        this.statistik = statistik;
    }

    public boolean isBestellen() {
        return bestellen;
    }

    public void setBestellen(boolean bestellen) {
        this.bestellen = bestellen;
    }

    public boolean isBuchen() {
        return buchen;
    }

    public void setBuchen(boolean buchen) {
        this.buchen = buchen;
    }

    public boolean isKalender() {
        return kalender;
    }

    public void setKalender(boolean kalender) {
        this.kalender = kalender;
    }

    public String neueRolle() {
        Rolle rolle = new Rolle(name, superadmin, admin, lagerbestand, kalender, bestellen, buchen, statistik);

        name = null;
        superadmin = false;
        admin = false;
        lagerbestand = false;
        kalender = false;
        bestellen = false;
        buchen = false;
        statistik = false;

        service.createRolle(rolle);

        return "rollen.xhtml?faces-redirect=true";
    }
}
