package at.htlpinkafeld.langoscharly.gui.converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author DominikT
 */
public class DateConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null) {
;

            if (string.contains(".")) {
                String[] dateArray = string.split("\\.");

                String day = dateArray[0];
                String month = dateArray[1];
                String year = dateArray[2];

                return LocalDate.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
            }

        }

        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            LocalDate d = (LocalDate) o;

            return d.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        }

        return null;
    }

}
