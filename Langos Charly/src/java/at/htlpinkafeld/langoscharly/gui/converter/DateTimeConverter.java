package at.htlpinkafeld.langoscharly.gui.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author DominikT
 */
public class DateTimeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null) {

            String[] fullDate = string.split(" ");
            String date = fullDate[0];
            String time = fullDate[1];

            if (date.contains(".") && time.contains(":")) {
                String[] dateArray = date.split("\\.");
                String[] timeArray = time.split(":");

                String day = dateArray[0];
                String month = dateArray[1];
                String year = dateArray[2];
                
                String hours = timeArray[0];
                String mins = timeArray[1];

                return LocalDateTime.of(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), Integer.parseInt(hours), Integer.parseInt(mins), 0);
            }

        }

        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            LocalDateTime d = (LocalDateTime) o;

            return d.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
        }

        return null;
    }

}