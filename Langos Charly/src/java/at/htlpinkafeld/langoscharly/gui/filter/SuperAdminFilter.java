package at.htlpinkafeld.langoscharly.gui.filter;

import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author CoolarPro
 */
public class SuperAdminFilter implements Filter {

    private static final Logger logger = Logger.getLogger(AdminFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        logger.debug("SuperAdmin Filter:");

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);

        if (session != null && loggedIn) {
            User user = (User) session.getAttribute("user");

            if (user.getRolle().isSuperadmin()) {
                chain.doFilter(req, res);
                logger.debug("SuperAdmin Filter leitet weiter");
            } else {
                logger.debug("SuperAdmin Filter verhindert weiterleitung");
                String url = LangosCharlyService.BASE_URL;
                response.sendRedirect(response.encodeRedirectURL(url));
            }

        }
    }

    @Override
    public void destroy() {
    }

}
