
package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import java.io.InputStream;

/**
 *
 * @author Kolar
 */
public interface HauptspeisenDAO extends BaseDAO<Hauptspeise>{
    
    public InputStream readHauptspeisenImageByID(int id);
    public boolean updateImage(int id, InputStream bild);

}
