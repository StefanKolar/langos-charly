package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Langos;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Kolar
 */
public class LangosBean implements BaseBean {

    private LangosCharlyService service;

    private String name, beschreibung, allergene, bemerkung;
    private int mindestbestand;
    private boolean verfuegbar;
    private InputStream bild;

    public LangosBean() {
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public List<Langos> getLangos() {
        return DAOFactory.getInstance().readAllLangos();
    }

    public String saveLangos(Langos langos) {
        service.updateLangos(langos);
        return null;
    }

    public String deleteLangos(Langos langos) {
        service.deleteLangos(langos);

        return null;
    }

    public void updateLangos(Langos langos) {
        if (bild != null) {
            service.updateImage(langos, bild);
        }
        service.updateLangos(langos);
    }

    public LangosCharlyService getService() {
        return service;
    }

    public void setService(LangosCharlyService service) {
        this.service = service;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getAllergene() {
        return allergene;
    }

    public void setAllergene(String allergene) {
        this.allergene = allergene;
    }

    public InputStream getBild() {
        return bild;
    }

    public void setBild(InputStream bild) {
        this.bild = bild;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public int getMindestbestand() {
        return mindestbestand;
    }

    public void setMindestbestand(int mindestbestand) {
        this.mindestbestand = mindestbestand;
    }

    public boolean isVerfuegbar() {
        return verfuegbar;
    }

    public void setVerfuegbar(boolean verfuegbar) {
        this.verfuegbar = verfuegbar;
    }

    public void fileUpload(FileUploadEvent event) {
        try {
            bild = event.getFile().getInputstream();
            
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("Bild hochgeladen", new FacesMessage("Erfolgreich", "Bild wurde erfolgreich hochgeladen"));
        } catch (IOException e) {
            Logger.getLogger(LangosBean.class).fatal("Bild nicht möglich hochzuladen: " + e.getMessage(), e);
        }

    }
    
    public void cancel(){
        bild = null;
    }

    public String neuesLangos() {

        Langos langos = new Langos(name, beschreibung, allergene, mindestbestand, verfuegbar, bemerkung);

        try {
            service.createLangos(langos);

            name = null;
            beschreibung = null;
            allergene = null;
            bild = null;
            mindestbestand = 0;
            verfuegbar = false;
            bemerkung = null;

            return "langos.xhtml?faces-redirect=true";
        } catch (UserException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(ex.getSeverity(), "Ein Problem ist Aufgetreten", ""));

        }

        return null;

    }

    public List<Langos> getVerfuegbareLangos() {
        return this.service.getVerfuegbareLangos();
    }

    public String getNameofLangosByID(int id) {
        return this.service.getLangos(id).getName();
    }

}
