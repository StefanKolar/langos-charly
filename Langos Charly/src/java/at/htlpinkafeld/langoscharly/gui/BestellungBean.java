package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Bestellposition;
import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author DominikT
 */
public class BestellungBean implements BaseBean {

    private LangosCharlyService service;
    private LocalDate datum;
    private String ort;
    private int plz;
    private String adresse;
    private int anz;
    private List<Integer> menge = new LinkedList<>();
    private List<Integer> mengeCopy = new LinkedList<>();
    private Bestellung bestellung;
    private List<Bestellposition> bestellpositionen = new LinkedList<>();
    private boolean rendered = false;
    private Bestellposition bestellposition;
    private List<Bestellung> bestellungen = new LinkedList<>();
    private Map<String, Integer> confirmMap = new HashMap<>();

    public BestellungBean() {
    }

    public int getAnz() {
        return anz;
    }

    public void setAnz(int anz) {
        this.anz = anz;
        this.menge.add(this.anz);
        this.mengeCopy.add(this.anz);
    }

//    public int getMin() {
//        if (!this.confirmMap.isEmpty()) {
//            for (int i = 0, j = 0; i < this.service.getVerfuegbareLangos().size(); i++, j++) {
//                if (this.service.getVerfuegbareLangos().get(i).getName().equals(this.confirmMap.entrySet().stream().collect(Collectors.toList()).get(j).getKey())) {
//                    return this.confirmMap.entrySet().stream().collect(Collectors.toList()).get(j).getValue();
//                } else {
//                    j--;
//                }
//            }
//        }
//        return 0;
//    }
    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<Integer> getMenge() {
        return menge;
    }

    public void setMenge(List<Integer> menge) {
        this.menge = menge;
    }

    public List<Bestellung> getBestellungen() {
        try {
            this.bestellungen = this.service.getBestellungen();
            return this.bestellungen;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Keine Daten zum anzeigen vorhanden"));
        }
        return null;
    }

    public void setBestellungen(List<Bestellung> bestellungen) {
        this.bestellungen = bestellungen;
    }

    public List<Bestellung> getBestellungenByCurrentUser() {
        return this.service.getBestellungenByUser(this.service.getCurrentUser()).stream().sorted((b1, b2) -> b2.getDatum().compareTo(b1.getDatum())).collect(Collectors.toList());
    }

    public LocalDate getMinDate() {
        return LocalDate.now().plusDays(3);
    }

    private void sendMail() {
        User user = this.service.getCurrentUser();

        if (user != null) {

            StringBuilder langosBuilder = new StringBuilder();

            langosBuilder.append("Lieferdatum: ").append(bestellung.getDatum().format(DateTimeFormatter.ofPattern("dd LLLL yyyy"))).append("\n");

            for (Bestellposition bp : getBestellpositionenByBestellID(bestellung.getBestellID())) {
                langosBuilder.append(this.service.getLangos(bp.getLangosID()).getName()).append(": \t").append(bp.getMenge()).append("\n");
            }

            String text = service.getInformation(5).getText();
            StringBuilder username = new StringBuilder();

            switch (user.getTyp()) {
                case PRIVAT:
                    username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                    break;
                case FIRMA:
                    username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                    break;
                case VEREIN:
                    username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                    break;
            }

            text = text.replace("USERNAME", username.toString()).replace("DETAILS", langosBuilder.toString());

            MailClient.sendMail(user.getEmail(), "Anfragebestätigung", text);
            MailClient.sendMail("Neue Bestellanfrage", "Sehr geehrtes Admin-Team!\n\nEin neue Bestellung ist im System mit folgenden Datum: " + bestellung.getDatum().format(DateTimeFormatter.ofPattern("dd LLLL yyyy")) + ".\n\n[Diese Email wird automatisch generier]");
        }
    }

    public String bestellen() {
        try {
            bestellung = this.service.createBestellung(new Bestellung(this.service.getCurrentUser().getID(), this.datum, this.ort, this.plz, this.adresse));
            for (int i = 0; i < this.service.getVerfuegbareLangos().size(); i++) {
                if (this.menge.get(i) > 0) {
                    this.service.createBestellposition(new Bestellposition(bestellung.getBestellID(), this.service.getLangos().get(i).getID(), this.menge.get(i)));
                }
            }

            new Thread(() -> {
                try {
                    if (bestellung != null) {
                        sendMail();
                    }
                } catch (Exception e) {
                    Logger.getLogger(this.getClass()).fatal("Mail konnte nicht gesendet werden.");
                }
            }).start();

        } catch (Exception e) {
            if (bestellung != null) {
                this.service.deleteBestellung(bestellung);
            }
            Logger.getLogger(BestellungBean.class).log(Level.FATAL, "Erstellen der Bestellung nicht erfolgreich!", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ein Fehler ist aufgetreten", "Die Bestellung konnte nicht ausgeführt werden"));
            return null;
        }
        this.menge.clear();

        return "bestellUebersicht.xhtml?faces-redirect=true";

    }

    public Bestellposition getBestellposition() {
        return bestellposition;
    }

    public void setBestellposition(Bestellposition bestellposition) {
        this.bestellposition = bestellposition;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public List<Bestellposition> getBestellpositionen() {
        return this.bestellpositionen;
    }

    public boolean isRendered() {
        return rendered;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public void setBestellpositionen(List<Bestellposition> bestellpositionen) {
        this.bestellpositionen = bestellpositionen;
    }

    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    public void deleteBestellung(Bestellung b) {
        this.service.deleteBestellung(b);
    }

    public void deleteBestellungByAdmin(Bestellung b) {
        this.service.deleteBestellung(b);

        StringBuilder username = new StringBuilder();

        User user = this.service.getUser(this.bestellung.getUserID());

        switch (user.getTyp()) {
            case PRIVAT:
                username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                break;
            case FIRMA:
                username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                break;
            case VEREIN:
                username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                break;
        }

        MailClient.sendMail(user.getEmail(), "Absage der Langos Bestellung", this.service.getInformation(10).getText().replace("AKTIVITÄT", "Bestellung").replace("USERNAME", username.toString()));
    }

    public List<Bestellposition> getBestellpositionenByBestellID(int id) {
        this.bestellpositionen = this.service.getBestellpositionenByBestellID(id);

        return this.bestellpositionen;
    }

    public void updateBestellung(Bestellung bestellung) {
        service.updateBestellung(bestellung);
    }

    public void updateBestellPosition(Bestellposition bestellposition) {
        this.service.updateBestellposition(bestellposition);
    }

    public String addUserLangos() {
        reset();

        for (int i = 0; i < this.service.getVerfuegbareLangos().size(); i++) {
            if (this.mengeCopy.get(i) > 0) {
                this.confirmMap.put(this.service.getVerfuegbareLangos().get(i).getName(), this.mengeCopy.get(i));
            }
        }

        this.mengeCopy.clear();

        return "anschrift.xhtml?faces-redirect=true";
    }

//    public int getPlaceholder() {
////        if (this.confirmMap != null) {
////            for (Entry<String, Integer> entry : this.confirmMap.entrySet()) {
////                for(int i = 0; i < this.service.getVerfuegbareLangos().size(); i++) {
////                    if(this.service.getVerfuegbareLangos().get(i).getName().equals(entry.getKey())) {
////                        setAnz(entry.getValue());
////                        return this.anz;
////                    }
////                }
////            }
////        }
////        return 0;
//        return 0;
//
//    }
//
    public void reset() {
        this.confirmMap.keySet().clear();
        this.confirmMap.values().clear();
        this.confirmMap.clear();
        this.anz = 0;
    }

    public Set<Entry<String, Integer>> getEntrySet() {
        return this.confirmMap.entrySet();
    }

    public LangosCharlyService getService() {
        return service;
    }

    public Map<String, Integer> getConfirmMap() {
        return this.confirmMap;
    }

    public Set<Entry<String, Integer>> getBestandSet() {
        return null;
    }

    public void setConfirmMap(Map<String, Integer> confirmMap) {
        this.confirmMap = confirmMap;
    }

    public void setBestellungForDetails(Bestellung b) {
        this.bestellung = b;

        this.bestellpositionen = this.service.getBestellpositionenByBestellID(b.getBestellID());

    }

    public String bestaetigen() {
        this.bestellung.setBestaetigt(true);

        this.service.updateBestellung(bestellung);

        this.service.addEvent(this.bestellung);

        this.service.decreaseBestandByBestellung(this.bestellung);

        return "/kalender/kalender.xhtml?faces-redirect=true";
    }

}
