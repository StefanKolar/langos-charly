package at.htlpinkafeld.langoscharly.pojo.enums;

/**
 *
 * @author Kolar
 */
public enum Typ {

    FIRMA(0, "Firma"),
    VEREIN(1, "Verein"),
    PRIVAT(2, "Privat");

    private final int id;
    private final String name;
    
    private Typ(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public static Typ getTyp(int id){
        for(Typ typ : values()){
            if(typ.getID() == id)
                return typ;
        }
        
        return null;
    }
    
    public static Typ getTyp(String name){
        for(Typ typ : values()){
            if(typ.getName().equals(name))
                return typ;
        }
        
        return null;
    }
    
    
    
}
