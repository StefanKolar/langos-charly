package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author CoolarPro
 */
public class HauptspeisenJdbcDAOTest implements TestDAOClass {

    private List<Hauptspeise> hauptspeisen = new LinkedList<>();

    @Before
    public void init() {
        hauptspeisen.add(new Hauptspeise("Kolar1", "Ein Test Hauptspeise", "A,C,B", false, ""));
        hauptspeisen.add(new Hauptspeise("Kolar2", "Ein Test Hauptspeise", "A,C,B", false, ""));
        hauptspeisen.add(new Hauptspeise("Kolar3", "Ein Test Hauptspeise", "A,C,B", false, ""));
        hauptspeisen.add(new Hauptspeise("Kolar4", "Ein Test Hauptspeise", "A,C,B", false, ""));

        for (Hauptspeise hs : hauptspeisen) {
            DAOFactory.getInstance().createHauptspeise(hs);
        }
    }

    @After
    public void delete() {
        for (Hauptspeise hs : hauptspeisen) {
            DAOFactory.getInstance().deleteHauptspeise(hs);
        }
    }

    @Test
    @Override
    public void testUpdate() {
        assertEquals("Hauptspeise Without Update", hauptspeisen.get(0), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(0).getID()));
        hauptspeisen.get(0).setName("Update Hauptspeise Kolar");
        assertTrue("Update Hauptspeise", DAOFactory.getInstance().updateHauptspeise(hauptspeisen.get(0)));
        assertEquals("Update Hauptspeise Confirmation", hauptspeisen.get(0), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(0).getID()));
    }

    @Test
    @Override
    public void testRead() {
        assertEquals("Read Hauptspeise 1", hauptspeisen.get(0), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(0).getID()));
        assertEquals("Read Hauptspeise 2", hauptspeisen.get(1), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(1).getID()));
        assertEquals("Read Hauptspeise 3", hauptspeisen.get(2), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(2).getID()));
        assertEquals("Read Hauptspeise 4", hauptspeisen.get(3), DAOFactory.getInstance().readHauptspeise(hauptspeisen.get(3).getID()));
    }

    @Test
    @Override
    public void testInsert() {
        Hauptspeise hauptseise = new Hauptspeise("Kolar5", "Ein Test Hauptspeise", "A,C,B", false, "");
        this.hauptspeisen.add(hauptseise);
        assertTrue("Insert Hauptspeise", DAOFactory.getInstance().createHauptspeise(hauptseise) != null);
    }

    @Test
    @Override
    public void testDelete() {
        assertTrue("Delete Hauptspeise 1", DAOFactory.getInstance().deleteHauptspeise(hauptspeisen.get(0)));
        assertTrue("Delete Hauptspeise 2", DAOFactory.getInstance().deleteHauptspeise(hauptspeisen.get(1)));
        assertTrue("Delete Hauptspeise 3", DAOFactory.getInstance().deleteHauptspeise(hauptspeisen.get(2)));
        assertTrue("Delete Hauptspeise 4", DAOFactory.getInstance().deleteHauptspeise(hauptspeisen.get(3)));

        hauptspeisen.clear();
    }
}

