package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.pojo.Hauptspeise;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author kevinunger
 */
public class FestBuchenJdbcDAOTest implements TestDAOClass {

    private List<Fest> feste = new LinkedList<>();

    @Before
    public void init() {
        feste.add(new Fest(1, 1, "KolarsFest", "Stefan", "Kolar", "email", "12345678", "Stegersbach", 7551, "Stabelweg2", LocalDateTime.now().plusDays(4), LocalDateTime.now().plusDays(4), LocalDateTime.now().plusDays(4), "16kW", 44, 1, 1, "anmerkungen", true, false));
        feste.add(new Fest(2, 2, "KevinsFest", "Kevin", "Unger", "kevin.unger@htlpinkafeld.at", "12345678", "Stegersbach", 7551, "Grazer Str", LocalDateTime.now().plusDays(4), LocalDateTime.now().plusDays(4), LocalDateTime.now().plusDays(4), "16kW", 44, 1, 1, "meine anmerkungen", true, false));


        for (Fest f : feste) {
            DAOFactory.getInstance().createBuchung(f);
        }
    }

    @After
    public void delete() {
        for (Fest f : feste) {
            DAOFactory.getInstance().deleteBuchung(f);
        }
    }

    @Test
    @Override
    public void testUpdate() {
        assertEquals("Fest Without Update", feste.get(0), DAOFactory.getInstance().readBuchung(feste.get(0).getID()));
        feste.get(0).setFestName("Update Fest Kolar");
        assertTrue("Update Fest", DAOFactory.getInstance().updateBuchung(feste.get(0)));
        assertEquals("Update Fest Confirmation", feste.get(0), DAOFactory.getInstance().readBuchung(feste.get(0).getID()));
    }

    @Test
    @Override
    public void testRead() {
        assertEquals("Read Fest 1", feste.get(0), DAOFactory.getInstance().readBuchung(feste.get(0).getID()));
        assertEquals("Read Fest 2", feste.get(1), DAOFactory.getInstance().readBuchung(feste.get(1).getID()));
    }

    @Test
    @Override
    public void testInsert() {
        Fest fest = new Fest(1, 1, "DominiksFest", "Dominik", "Tomsits", "email", "12345678", "Stegersbach", 7551, "Stab2", LocalDateTime.now().plusDays(5), LocalDateTime.now().plusDays(6), LocalDateTime.now().plusDays(7), "16kW", 44, 1, 1, "anmerkungen", true, false);
        this.feste.add(fest);
        assertTrue("Insert Fest", DAOFactory.getInstance().createBuchung(fest) != null);
    }

    @Test
    @Override
    public void testDelete() {
        assertTrue("Delete Fest 1", DAOFactory.getInstance().deleteBuchung(feste.get(0)));
        assertTrue("Delete Fest 2", DAOFactory.getInstance().deleteBuchung(feste.get(1)));
        
        feste.clear();
    }
}

