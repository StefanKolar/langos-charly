package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.interfaces.ImageDAO;
import at.htlpinkafeld.langoscharly.pojo.Image;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class ImageJdbcDAO extends BaseJdbcDAO<Image> implements ImageDAO {

    public ImageJdbcDAO() {
        super("Bilder", "BildID");
    }

    @Override
    protected Image getPojoFromResultSet(ResultSet result) throws SQLException {

        int bildID = result.getInt(getPkName());
        String name = result.getString("Name");
        InputStream bild = result.getBinaryStream("Bild");

        return new Image(bildID, name, bild);
    }

    @Override
    public InputStream readImageBildByID(int id) {

        String sql = "SELECT Bild FROM " + super.getTablename() + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        return result.getBinaryStream("Bild");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return null;
    }

    @Override
    public boolean updateImage(int id, InputStream bild) {

        String sql = "UPDATE " + super.getTablename() + " set Bild = ?" + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setBinaryStream(1, bild);
                statement.setInt(2, id);
                if (statement.executeUpdate() > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Image i) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Bild = ? where " + getPkName() + " = ?";

        PreparedStatement statement = c.prepareStatement(sql);
        statement.setBinaryStream(1, i.getBild());
        statement.setInt(2, i.getID());

        return statement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Image i) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Name, Bild,) VALUES (?,?)";
        PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, i.getName());
        statement.setBinaryStream(2, i.getBild());

        return statement;

    }

}
