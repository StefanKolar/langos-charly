package at.htlpinkafeld.langoscharly.pojo;

/**
 *
 * @author kevinunger
 */
public class FestSpeisen implements Identifiable
{
    private int festID;
    private int hauptspeiseID;
    private int menge;

    public FestSpeisen() {
    }

    public FestSpeisen(int festID, int hauptspeiseID, int menge) {
        this.festID = festID;
        this.hauptspeiseID = hauptspeiseID;
        this.menge = menge;
    }
    
    @Override
    public void setID(int id) {
        this.festID = id;
    }

    @Override
    public int getID() {
        return this.festID;
    }

    public int getHauptspeiseID() {
        return hauptspeiseID;
    }

    public void setHauptspeiseID(int hauptspeiseID) {
        this.hauptspeiseID = hauptspeiseID;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.festID;
        hash = 89 * hash + this.hauptspeiseID;
        hash = 89 * hash + this.menge;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FestSpeisen other = (FestSpeisen) obj;
        if (this.festID != other.festID) {
            return false;
        }
        if (this.hauptspeiseID != other.hauptspeiseID) {
            return false;
        }
        if (this.menge != other.menge) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FestBuchenHauptspeise{" + "festID=" + festID + ", hauptspeiseID=" + hauptspeiseID + ", menge=" + menge + '}';
    }
    
    
    
}
