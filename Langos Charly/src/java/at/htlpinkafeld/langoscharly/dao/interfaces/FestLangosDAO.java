package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import java.util.List;

/**
 *
 * @author kevinunger
 */
public interface FestLangosDAO extends BaseDAO<FestLangos>{
    public List<FestLangos> readFestLangosByFestID(int id);
    
}
