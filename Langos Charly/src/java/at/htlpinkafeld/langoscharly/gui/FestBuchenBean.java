package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Fest;
import at.htlpinkafeld.langoscharly.pojo.FestLangos;
import at.htlpinkafeld.langoscharly.pojo.FestSpeisen;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import at.htlpinkafeld.langoscharly.utilities.MailClient;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author kevinunger
 */
public class FestBuchenBean implements BaseBean {

    private Integer besucherAnzahl_holder;
    private Integer plz_holder;

    private int userID;
    private String festName;
    private String vorname;
    private String nachname;
    private String telefonnummer;
    private String email;
    private int besucherAnzahl;
    private boolean speisekarte_holder; //1 für LC
    private boolean kassa_holder; //1 für LC
    private int speisekarte;
    private int kassa;
    private LocalDateTime veranstaltungsBeginn;
    private LocalDateTime veranstaltungsEnde;
    private LocalDateTime aufstellDatum;
    private String anmerkungen;
    private String ort;
    private int plz;
    private String adresse;
    private String stromanschluss;
    private boolean bestaetigt;
    private boolean abgeschlossen;

    private Fest buchung;
    private List<Fest> buchungen = new LinkedList<>();

    private List<String> langos = new ArrayList<>();
    private List<String> hauptspeise = new ArrayList<>();

    private LangosCharlyService service;

    private List<Integer> mengeLangos = new LinkedList<>();
    private List<Integer> mengeHauptspeise = new LinkedList<>();
    private int anzLangos;
    private int anzHauptspeise;
    private FestLangos festLangos;
    private FestSpeisen festSpeisen;

    public FestBuchenBean() {
        this.besucherAnzahl_holder = null;
        this.plz_holder = null;

        this.kassa_holder = true;
        this.speisekarte_holder = true;
    }

    @PostConstruct
    public void init() {
        User user = service.getCurrentUser();

        //Angemeldeter user erscheint direkt auf inputText !
        if (!user.getFirmenname().equals(LangosCharlyService.GAST)) {
            vorname = user.getVorname();
            nachname = user.getNachname();
            telefonnummer = user.getTelefonnummer();
            email = user.getEmail();
        }
    }

    public void buchen() {
        //holder variablen sind nötig, sonst steht eine 0 im inputText anstatt dem placeholder !
        if (besucherAnzahl_holder == null) {
            besucherAnzahl = 0;
        } else {
            besucherAnzahl = besucherAnzahl_holder;
        }

        if (plz_holder == null) {
            plz = 0;
        } else {
            plz = plz_holder;
        }

        if (speisekarte_holder == true) {
            speisekarte = 1;
        } else {
            speisekarte = 0;
        }

        if (kassa_holder == true) {
            kassa = 1;
        } else {
            kassa = 0;
        }

        try {
            //Langos in Anmerkungen speichern
            StringBuilder langosBuilder = new StringBuilder();

            for (int i = 0; i < this.getLangos().size(); i++) {
                langosBuilder.append(this.langos.get(i)).append("\n");
            }
            this.anmerkungen += "\n\nBestellte Langos:\n"
                    + langosBuilder.toString();

            //Hauptspeise in Anmerkungen speichern
            StringBuilder hauptspeiseBuilder = new StringBuilder();

            for (int i = 0; i < this.getHauptspeise().size(); i++) {
                hauptspeiseBuilder.append(this.hauptspeise.get(i)).append("\n");
            }
            this.anmerkungen += "\n\nBestellte Hauptspeisen:\n"
                    + hauptspeiseBuilder.toString();

            //fest in DB speichern
            try {
                buchung = this.service.createFest(new Fest(this.service.getCurrentUser().getID(), this.festName, this.vorname, this.nachname, this.telefonnummer, this.email, this.besucherAnzahl, this.speisekarte, this.kassa, this.veranstaltungsBeginn, this.veranstaltungsEnde, this.aufstellDatum, this.anmerkungen, this.ort, this.plz, this.adresse, this.stromanschluss));
            } catch (UserException ex) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(ex.getSeverity(), "Fehler bei der Buchung", ex.getMessage()));
            }

        } catch (Exception e) {
            Logger.getLogger(BestellungBean.class).log(Level.FATAL, "Erstellen der Buchung nicht erfolgreich!", e);
        }

        new Thread(() -> sendMail(buchung)).start();

        //nach klick auf "Fest buchen" alle eingaben clearen
        this.festName = null;
        this.vorname = null;
        this.nachname = null;
        this.telefonnummer = null;
        this.email = null;
        this.besucherAnzahl_holder = null;
        this.veranstaltungsBeginn = null;
        this.veranstaltungsEnde = null;
        this.aufstellDatum = null;
        this.anmerkungen = null;
        this.ort = null;
        this.plz_holder = null;
        this.adresse = null;

        try {
            service.getToBase();
        } catch (IOException ex) {
        }
    }

    private void sendMail(Fest fest) {
        User user = this.service.getCurrentUser();

        StringBuilder text = new StringBuilder();

        if (user.getRolle().getID() != LangosCharlyService.GAST_ROLLEN_NUMBER) {

            switch (user.getTyp()) {
                case PRIVAT:
                    text.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                    break;
                case FIRMA:
                    text.append("Sehr geehrte Firma ").append(user.getFirmenname());
                    break;
                case VEREIN:
                    text.append("Sehr geehrter Verein ").append(user.getFirmenname());
                    break;
            }
        } else {
            text.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
        }
        text.append(
                "!\n\nDanke für das Buchen Ihres Festes.\n Ihre Anfrage wird schnellstmöglichst bearbeitet und wir melden uns in Kürze bei Ihnen.\n").append(
                        "Anbei finden Sie nochmals Ihre Anfrage:\n").append("\n").append(
                "Wir können bei Ihnen aufbauen am: ").append(fest.getAufstellDatum().format(DateTimeFormatter.ofPattern("dd.MM.yyyy:HH:mm"))).append(
                "\nFestbeginn: ").append(fest.getVeranstaltungsBeginn().format(DateTimeFormatter.ofPattern("dd.MM.yyyy:HH:mm"))).append(
                "\nFestende: ").append(fest.getVeranstaltungsEnde().format(DateTimeFormatter.ofPattern("dd.MM.yyyy:HH:mm"))).append(
                "\nVerfügbarer Stromanschluss: ").append(fest.getStromanschluss()).append(" Kw").append(
                "\n\nAnmerkungen: ").append(fest.getAnmerkungen()).append(
                "\n\nMit freundlichen Grüßen,\nIhr Langos Charly Team");

        MailClient.sendMail(fest.getEmail(), "Festanfrage", text.toString());
        MailClient.sendMail("Neue Festanfrage", "Sehr geehrtes Admin-Team!\n\nEin neues Fest ist im System mit folgenden Datum: " + fest.getVeranstaltungsBeginn()+ ".\n\n[Diese Email wird automatisch generier]");
    }

    public List<Fest> getAllFeste() {
        try {
            return this.service.getFeste();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Keine Daten zum anzeigen vorhanden"));
        }
        return null;
    }

    public void updateFest(Fest buchung) {
        service.updateFest(buchung);
    }

    public void deleteFest(Fest f) {
        User user = service.getUser(f.getUserID());
        String text = service.getInformation(10).getText();
        StringBuilder username = new StringBuilder();

        switch (user.getTyp()) {
            case PRIVAT:
                username.append("Sehr geehrte/r ").append(user.getVorname()).append(" ").append(user.getNachname());
                break;
            case FIRMA:
                username.append("Sehr geehrte Firma ").append(user.getFirmenname());
                break;
            case VEREIN:
                username.append("Sehr geehrter Verein ").append(user.getFirmenname());
                break;
        }

        text = text.replace("USERNAME", username.toString()).replace("AKTIVITÄT", "Fest: " + f.getFestName());

        MailClient.sendMail(user.getEmail(), "Absage", text);
        this.service.deleteFest(f);

    }

    public void bestaetigen() {
        this.buchung.setBestaetigt(true);
        this.service.updateFest(buchung);

        this.service.addEvent(this.buchung);
    }

    public void abschliessen() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect( ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).encodeRedirectURL("/admin/abschliessen.xhtml?faces-redirect=true"));

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FestBuchenBean.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public void abschliessen2() {
        //Abgeschlossenes Fest (Langos) für Statistik in DB speichern
        for (int i = 0; i < this.mengeLangos.size(); i++) {
            //0 Werte (Menge) in DB verhindern
            if (mengeLangos.get(i) != 0) {
                try {
                    festLangos = this.service.createFestLangos(new FestLangos(buchung.getFestBuchenID(), this.service.getLangos().get(i).getID(), mengeLangos.get(i)));
                } catch (UserException ex) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(ex.getSeverity(), "Fehler bei Statistikeintrag Langos", ex.getMessage()));
                }
            }
        }
        //Abgeschlossenes Fest (Hauptspeise) für Statistik in DB speichern
        for (int j = 0; j < this.mengeHauptspeise.size(); j++) {
            //0 Werte (Menge) in DB verhindern
            if (mengeHauptspeise.get(j) != 0) {
                try {
                    festSpeisen = this.service.createFestSpeisen(new FestSpeisen(buchung.getFestBuchenID(), this.service.getHauptspeisen().get(j).getID(), mengeHauptspeise.get(j)));
                } catch (UserException ex) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(ex.getSeverity(), "Fehler bei Statistikeintrag Speisen", ex.getMessage()));

                }
            }

        }
        this.mengeLangos.clear();
        this.mengeHauptspeise.clear();

        this.buchung.setAbgeschlossen(true);
        this.service.updateFest(buchung);
        this.service.decreaseBestandByFest(buchung);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect( ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse()).encodeRedirectURL("/admin/festBuchenUebersicht.xhtml?faces-redirect=true"));

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FestBuchenBean.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public void ueberUns() {
        try {
            service.getToBase();

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FestBuchenBean.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public int getAnzLangos() {
        return anzLangos;
    }

    public boolean buttonAbschliessen() {
        if (this.buchung != null) {
            return this.buchung.isBestaetigt();
        }
        return false;
    }

    public void setAnzLangos(int anzLangos) {
        this.anzLangos = anzLangos;

        this.mengeLangos.add(this.anzLangos);
    }

    public int getAnzHauptspeise() {
        return anzHauptspeise;
    }

    public void setAnzHauptspeise(int anzHauptspeise) {
        this.anzHauptspeise = anzHauptspeise;

        this.mengeHauptspeise.add(this.anzHauptspeise);
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFestName() {
        return festName;
    }

    public void setFestName(String festName) {
        this.festName = festName;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBesucherAnzahl() {
        return besucherAnzahl;
    }

    public void setBesucherAnzahl(int besucherAnzahl) {
        this.besucherAnzahl = besucherAnzahl;
    }

    public int getSpeisekarte() {
        return speisekarte;
    }

    public void setSpeisekarte(int speisekarte) {
        this.speisekarte = speisekarte;
    }

    public int getKassa() {
        return kassa;
    }

    public void setKassa(int kassa) {
        this.kassa = kassa;
    }

    public LocalDateTime getVeranstaltungsBeginn() {
        return veranstaltungsBeginn;
    }

    public void setVeranstaltungsBeginn(LocalDateTime veranstaltungsBeginn) {
        this.veranstaltungsBeginn = veranstaltungsBeginn;
    }

    public LocalDateTime getVeranstaltungsEnde() {
        return veranstaltungsEnde;
    }

    public void setVeranstaltungsEnde(LocalDateTime veranstaltungsEnde) {
        this.veranstaltungsEnde = veranstaltungsEnde;
    }

    public LocalDateTime getAufstellDatum() {
        return aufstellDatum;
    }

    public void setAufstellDatum(LocalDateTime aufstellDatum) {
        this.aufstellDatum = aufstellDatum;
    }

    public String getAnmerkungen() {
        return anmerkungen;
    }

    public void setAnmerkungen(String anmerkungen) {
        this.anmerkungen = anmerkungen;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getStromanschluss() {
        return stromanschluss;
    }

    public void setStromanschluss(String stromanschluss) {
        this.stromanschluss = stromanschluss;
    }

    public boolean isBestaetigt() {
        return bestaetigt;
    }

    public void setBestaetigt(boolean bestaetigt) {
        this.bestaetigt = bestaetigt;
    }

    public boolean isAbgeschlossen() {
        return abgeschlossen;
    }

    public void setAbgeschlossen(boolean abgeschlossen) {
        this.abgeschlossen = abgeschlossen;
    }

    public Integer getBesucherAnzahl_holder() {
        return besucherAnzahl_holder;
    }

    public void setBesucherAnzahl_holder(Integer besucherAnzahl_holder) {
        this.besucherAnzahl_holder = besucherAnzahl_holder;
    }

    public Integer getPlz_holder() {
        return plz_holder;
    }

    public void setPlz_holder(Integer plz_holder) {
        this.plz_holder = plz_holder;
    }

    public boolean isSpeisekarte_holder() {
        return speisekarte_holder;
    }

    public void setSpeisekarte_holder(boolean speisekarte_holder) {
        this.speisekarte_holder = speisekarte_holder;
    }

    public boolean isKassa_holder() {
        return kassa_holder;
    }

    public void setKassa_holder(boolean kassa_holder) {
        this.kassa_holder = kassa_holder;
    }

    public List<String> getLangos() {
        return langos;
    }

    public void setLangos(List<String> langos) {
        this.langos = langos;
    }

    public List<String> getHauptspeise() {
        return hauptspeise;
    }

    public void setHauptspeise(List<String> hauptspeise) {
        this.hauptspeise = hauptspeise;
    }

    public Fest getBuchung() {
        return buchung;
    }

    public void setBuchung(Fest buchung) {
        this.buchung = buchung;
    }

    public List<Fest> getBuchungen() {
        return buchungen;
    }

    public void setBuchungen(List<Fest> buchungen) {
        this.buchungen = buchungen;
    }

    public LocalDateTime getMinDate() {
        return LocalDate.now().plusDays(3).atStartOfDay();
    }

    
}
