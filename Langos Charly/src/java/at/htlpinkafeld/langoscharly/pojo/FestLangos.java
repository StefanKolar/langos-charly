package at.htlpinkafeld.langoscharly.pojo;

/**
 *
 * @author kevinunger
 */
public class FestLangos implements Identifiable
{
    private int festID;
    private int langosID;
    private int menge;

    public FestLangos() {
    }

    public FestLangos(int festID, int langosID, int menge) {
        this.festID = festID;
        this.langosID = langosID;
        this.menge = menge;
    }
        

    @Override
    public void setID(int id) {
        this.festID = id;
    }

    @Override
    public int getID() {  
        return this.festID;
    }

    public int getLangosID() {
        return langosID;
    }

    public void setLangosID(int langosID) {
        this.langosID = langosID;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.festID;
        hash = 97 * hash + this.langosID;
        hash = 97 * hash + this.menge;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FestLangos other = (FestLangos) obj;
        if (this.festID != other.festID) {
            return false;
        }
        if (this.langosID != other.langosID) {
            return false;
        }
        if (this.menge != other.menge) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FestBuchenLangos{" + "festID=" + festID + ", langosID=" + langosID + ", menge=" + menge + '}';
    }
    
    
    
}
