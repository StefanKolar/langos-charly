package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.connection.ConnectionManager;
import at.htlpinkafeld.langoscharly.dao.factory.DAOFactory;
import at.htlpinkafeld.langoscharly.dao.interfaces.UserDAO;
import at.htlpinkafeld.langoscharly.pojo.User;
import at.htlpinkafeld.langoscharly.pojo.enums.Typ;
import at.htlpinkafeld.langoscharly.utilities.UserException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.faces.application.FacesMessage;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Kolar
 */
public class UserJdbcDAO extends BaseJdbcDAO<User> implements UserDAO {

    public UserJdbcDAO() {
        super("Users", "UserID");
    }

    @Override
    public User readUserByEmail(String email) {
        User user = null;

        String sql = "SELECT * FROM " + super.getTablename() + " WHERE Email = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setString(1, email);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        if (contains(result.getInt(getPkName()))) {
                            return get(result.getInt(getPkName()));
                        }
                        user = getPojoFromResultSet(result);
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return user;
    }

    /**
     *
     * @param id The id of the User
     * @return The image of the User
     */
    @Override
    public InputStream readUserImageByID(int id) {

        String sql = "SELECT Bild FROM " + super.getTablename() + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setInt(1, id);
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        return result.getBinaryStream("Bild");
                    }
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return null;
    }

    @Override
    public boolean updateImage(int id, InputStream bild) {

        String sql = "UPDATE " + super.getTablename() + " set Bild = ?" + " WHERE " + super.getPkName() + " = ?";
        try {
            try (Connection connection = ConnectionManager.getInstance().getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {

                statement.setBinaryStream(1, bild);
                statement.setInt(2, id);
                if (statement.executeUpdate() > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserJdbcDAO.class).log(Level.FATAL, ex.getMessage(), ex);
            throw new UserException(FacesMessage.SEVERITY_ERROR, ex);
        }

        return false;
    }

    @Override
    protected User getPojoFromResultSet(ResultSet result) throws SQLException {

        int userID = result.getInt(getPkName());
        String firmenname = result.getString("Firmenname");
        int typ = result.getInt("Typ");
        String vorname = result.getString("Vorname");
        String nachname = result.getString("Nachname");
        int rollenID = result.getInt("RollenID");
        String email = result.getString("Email");
        String passwort = result.getString("Passwort");
        String telefonnummer = result.getString("Telefonnummer");
        String adresse = result.getString("Adresse");
        int plz = 0;
        try {
            plz = Integer.valueOf(result.getString("PLZ"));
        } catch (NumberFormatException ex) {
            // ignore
        }
        String ort = result.getString("Ort");

        return new User(userID, firmenname, Typ.getTyp(typ), vorname, nachname, DAOFactory.getInstance().readRolle(rollenID), email, passwort, telefonnummer, adresse, plz, ort);

    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, User user) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Firmenname = ?, Typ = ?, Vorname = ?, Nachname = ?, RollenID = ?, Email = ?, Passwort = ?, Telefonnummer = ?, Adresse = ?, PLZ = ?, Ort = ? where UserID = ?";

        PreparedStatement statement = c.prepareStatement(sql);

        statement.setString(1, user.getFirmenname());
        statement.setInt(2, user.getTyp().getID());
        statement.setString(3, user.getVorname());
        statement.setString(4, user.getNachname());
        statement.setInt(5, user.getRolle().getID());
        statement.setString(6, user.getEmail());
        statement.setString(7, user.getPasswort());
        statement.setString(8, user.getTelefonnummer());
        statement.setString(9, user.getAdresse());
        statement.setInt(10, user.getPlz());
        statement.setString(11, user.getOrt());
        statement.setInt(12, user.getID());

        return statement;

    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, User user) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Firmenname, Typ, Vorname, Nachname, Email, Passwort, Telefonnummer, Adresse, PLZ, Ort) VALUES (?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, user.getFirmenname());
        statement.setInt(2, user.getTyp().getID());
        statement.setString(3, user.getVorname());
        statement.setString(4, user.getNachname());
        statement.setString(5, user.getEmail());
        statement.setString(6, user.getPasswort());
        statement.setString(7, user.getTelefonnummer());
        statement.setString(8, user.getAdresse());
        statement.setInt(9, user.getPlz());
        statement.setString(10, user.getOrt());

        return statement;
    }

}
