package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.pojo.User;
import java.util.List;

/**
 *
 * @author DominikT
 */
public interface BestellDAO extends BaseDAO<Bestellung> {
    
    public List<Bestellung> readBestellungByUser(User user);
    
}
