package at.htlpinkafeld.langoscharly.gui.converter;

import at.htlpinkafeld.langoscharly.gui.RollenBean;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Kolar
 */
public class RollenConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        ValueExpression vex = fc.getApplication().getExpressionFactory().createValueExpression(fc.getELContext(), "#{rollenBean}", RollenBean.class);

        RollenBean bean = (RollenBean) vex.getValue(fc.getELContext());
        return bean.getRolleByName(string);

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((Rolle) o).getRolle();
    }

}
