package at.htlpinkafeld.langoscharly.dao.interfaces;

import at.htlpinkafeld.langoscharly.pojo.Image;
import java.io.InputStream;

/**
 *
 * @author Kolar
 */
public interface ImageDAO extends BaseDAO<Image> {

    public InputStream readImageBildByID(int id);
    public boolean updateImage(int id, InputStream bild);
}
