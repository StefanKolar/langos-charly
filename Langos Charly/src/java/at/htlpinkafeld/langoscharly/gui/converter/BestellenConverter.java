package at.htlpinkafeld.langoscharly.gui.converter;

import at.htlpinkafeld.langoscharly.pojo.Bestellung;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Kolar
 */
public class BestellenConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        ValueExpression vex = fc.getApplication().getExpressionFactory().createValueExpression(fc.getELContext(), "#{langosCharlyService}", LangosCharlyService.class);

        LangosCharlyService bean = (LangosCharlyService) vex.getValue(fc.getELContext());

        try {

            return bean.getBestellung(Integer.valueOf(string));

        } catch (NumberFormatException e) {

            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        try {
            return ((Bestellung) o).getID() + "";
        } catch (Exception e) {
            return null;
        }
    }

}
