
package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.io.Serializable;

/**
 *
 * @author Kolar
 */
public interface BaseBean extends Serializable{
    
    public LangosCharlyService getLangosCharlyService();
    public void setLangosCharlyService(LangosCharlyService service);

}
