package at.htlpinkafeld.langoscharly.gui;

import at.htlpinkafeld.langoscharly.pojo.Information;
import at.htlpinkafeld.langoscharly.service.LangosCharlyService;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Kolar
 */
public class InformationBean implements BaseBean {

    private LangosCharlyService service;

    private List<Information> informationen = new LinkedList<>();

    public InformationBean() {
    }

    @Override
    public LangosCharlyService getLangosCharlyService() {
        return service;
    }

    @Override
    public void setLangosCharlyService(LangosCharlyService service) {
        this.service = service;
    }

    public List<Information> getInformationen() {
        if (informationen.isEmpty()) {
            informationen = service.getInformationen();
        }
        return informationen;
    }

    public void updateInformation(Information information) {
        service.updateInformation(information);
    }

    public String getInformation(int id) {
        if (informationen.isEmpty()) {
            informationen = service.getInformationen();
        }
        return informationen.get(id).getText();
    }

}
