package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.interfaces.InformationenDAO;
import at.htlpinkafeld.langoscharly.pojo.Information;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kolar
 */
public class InformationJdbcDAO extends BaseJdbcDAO<Information> implements InformationenDAO {

    public InformationJdbcDAO() {
        super("Informationen", "InformationID");
    }

    @Override
    protected Information getPojoFromResultSet(ResultSet result) throws SQLException {

        int informationID = result.getInt(getPkName());
        String beschreibung = result.getString("Beschreibung");
        String text = result.getString("Text");

        return new Information(informationID, beschreibung, text);
    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Information t) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Text = ? where " + getPkName() + " = ?";

        PreparedStatement pStatement = c.prepareStatement(sql);

        pStatement.setString(1, t.getText());
        pStatement.setInt(2, t.getID());

        return pStatement;
    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Information t) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Beschreibung, Text) VALUES (?,?)";

        PreparedStatement pStatement = c.prepareStatement(sql);

        pStatement.setString(2, t.getBeschreibung());
        pStatement.setString(3, t.getText());

        return pStatement;
    }

}
