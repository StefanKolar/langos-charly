package at.htlpinkafeld.langoscharly.dao.jdcb;

import at.htlpinkafeld.langoscharly.dao.interfaces.RollenDAO;
import at.htlpinkafeld.langoscharly.pojo.Rolle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Kolar
 */
public class RollenJdbcDAO extends BaseJdbcDAO<Rolle> implements RollenDAO {

    public RollenJdbcDAO() {
        super("Rollen", "RollenID");
    }

    @Override
    protected Rolle getPojoFromResultSet(ResultSet result) throws SQLException {

        int rollenID = result.getInt(getPkName());
        String rolle = result.getString("Rolle");
        boolean superAdmin = result.getBoolean("SuperAdmin");
        boolean admin = result.getBoolean("Admin");
        boolean lagerbestand = result.getBoolean("Lagerbestand");
        boolean kalender = result.getBoolean("Kalender");
        boolean langosBestellen = result.getBoolean("LangosBestellen");
        boolean festBuchen = result.getBoolean("FestBuchen");
        boolean statistik = result.getBoolean("Statistik");

        return new Rolle(rollenID, rolle, superAdmin, admin, lagerbestand, kalender, langosBestellen, festBuchen, statistik);

    }

    @Override
    protected PreparedStatement getUpdateStatement(Connection c, Rolle r) throws SQLException {
        String sql = "UPDATE " + super.getTablename() + " set Rolle = ?, SuperAdmin = ?, admin = ?, Lagerbestand = ?, Kalender = ?, LangosBestellen = ?, FestBuchen = ?, Statistik = ? where " + getPkName() + " = ?";

        PreparedStatement statement = c.prepareStatement(sql);
        statement.setString(1, r.getRolle());
        statement.setBoolean(2, r.isSuperadmin());
        statement.setBoolean(3, r.isAdmin());
        statement.setBoolean(4, r.isLagerbestand());
        statement.setBoolean(5, r.isKalender());
        statement.setBoolean(6, r.isBestellen());
        statement.setBoolean(7, r.isBuchen());
        statement.setBoolean(8, r.isStatisik());
        statement.setInt(9, r.getID());

        return statement;

    }

    @Override
    protected PreparedStatement getInsertStatement(Connection c, Rolle r) throws SQLException {
        String sql = "INSERT INTO " + super.getTablename() + " (Rolle, SuperAdmin, Admin, Lagerbestand, Kalender, LangosBestellen, FestBuchen, Statistik) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, r.getRolle());
        statement.setBoolean(2, r.isSuperadmin());
        statement.setBoolean(3, r.isAdmin());
        statement.setBoolean(4, r.isLagerbestand());
        statement.setBoolean(5, r.isKalender());
        statement.setBoolean(6, r.isBestellen());
        statement.setBoolean(7, r.isBuchen());
        statement.setBoolean(8, r.isStatisik());

        return statement;
    }

}
